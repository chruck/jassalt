# salt/installArchAtopFuntoo/installBtrfs.sls

# Ensure `btrfs` installed so `btrfs` state modules can be used and
# `btrfs` mountpoint can be created.

include:
  - .identifyAsGentoo

{{sls}} - Make sure btrfs is installed:
  pkg.installed:
    - name: sys-fs/btrfs-progs
    # - provider: ebuildpkg  # should have worked, but now identifyAsGentoo
    - require:
      - installArchAtopFuntoo.identifyAsGentoo - Change identity to _g_entoo
      - installArchAtopFuntoo.identifyAsGentoo - Change identity to _G_entoo
