{% from "salt/map.jinja" import minionsvcID with context %}
{% from "pkgnames_map.jinja" import pkg_name with context %}

include:
  - gentoo
  - .gitfs
  - .minionsvc

{{sls}} - Install salt-minion pkg:
  pkg.installed:
    - name: {{pkg_name.salt_minion}}
    - refresh: True
    - require_in:
      - {{minionsvcID}}

{{sls}} - Set hash to SHA512:
  file.managed:
    - name: /etc/salt/minion.d/hash.conf
    - contents: "hash_type: sha512"
    - require:
      - {{sls}} - Install salt-minion pkg
    - watch_in:
      - {{minionsvcID}}
