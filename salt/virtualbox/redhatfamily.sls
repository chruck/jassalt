# Components needed for RedHat-family Linux

include:
  - .install

{% if "RedHat" == grains.os_family %}

{%   if "Fedora" == grains.os %}

{{sls}} - Install Virtualbox repo:
  pkgrepo.managed:
    - humanname: Fedora $releasever / $basearch -
    - name: virtualbox
    - gpgcheck: 1
    - gpgkey: https://www.virtualbox.org/download/oracle_vbox.asc
    - baseurl: http://download.virtualbox.org/virtualbox/rpm/fedora/$releasever/$basearch
    - require_in:
      - virtualbox.install - Install VirtualBox packages

{%   else %}  # non-Fedora:

# from https://download.virtualbox.org/virtualbox/rpm/el/virtualbox.repo
{{sls}} - Install Virtualbox repo:
  pkgrepo.managed:
    - humanname: Oracle Linux / RHEL / CentOS-$releasever / $basearch -
    - name: virtualbox
    - gpgcheck: 1
    - gpgkey: https://www.virtualbox.org/download/oracle_vbox.asc
    - baseurl: http://download.virtualbox.org/virtualbox/rpm/el/$releasever/$basearch
    - require_in:
      - virtualbox.install - Install VirtualBox packages

{%   endif %}  # Fedora
{% endif %}    # RedHat family
