# salt.salt.gitfs:  Setup gitfs for salt formulas on both master and
# minions.

{% from tpldir ~ "/map.jinja" import mastersvcID, minionsvcID with context %}
{% from "pkgnames_map.jinja" import pkg_name with context %}

include:
  - .salt-pip
  - .subdirs
  - .mastersvc
  - .minionsvc

{% set masterDir = "/etc/salt/master.d" %}
{% set minionDir = "/etc/salt/minion.d" %}

{% if "onedir" == grains.get("package", "") %}
{#
{% if not salt.cmd.retcode("salt-pip") %}

{%   set devpkgs = [] %}
{%   for devpkg in "gcc", "patchelf" %}
{%     if not salt.pkg.version(devpkg) %}
{%       do devpkgs.append(devpkg) %}
{%     endif %}  # pkg not installed
{%   endfor %}   # list of dev pkgs

{{sls}} - Install dev pkgs for pygit2:
  pkg.installed:
    - pkgs: {{devpkgs}}
    - onlyif: "! (salt-pip list |grep pygit2)"
#}

{{sls}} - Install pygit2:
  cmd.run:
    # pygit2 v1.14.1 per https://github.com/saltstack/salt/issues/66755
    # Hopefully to be fixed in 3007.2 .
    - name: salt-pip install pygit2==1.14.1
    - onlyif: "! (salt-pip list |grep pygit2)"
#    - runas: salt
#    - require:
#      - {{sls}} - Install dev pkgs for pygit2

{#
{{sls}} - Remove dev pkgs for pygit2, now that they're not needed:
  pkg.purged:
    - pkgs: {{devpkgs}}
    - require:
      - {{sls}} - Install pygit2
#}

{% else %}  # not OneDir version of Salt:

{{sls}} - Install pygit2:
  pkg.installed:
    - name: {{pkg_name.pygit}}
#    - watch_in:
#      - {{mastersvcID}}

{% endif %}  # OneDir Salt?

# Needed for masterless minion (eg, CICD testing minions)
{{sls}} - Set gitfs settings:
  file.managed:
    - names:
      - {{masterDir}}/gitfs.conf
      - {{minionDir}}/gitfs.conf
    - source: salt://salt/gitfs.conf
    - require:
      - salt.subdirs - Create salt subdirectories
      - {{sls}} - Install pygit2
    - watch_in:
      - {{mastersvcID}}
      - {{minionsvcID}}
