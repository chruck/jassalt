{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,
        stateNotInstalling,
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        mntFstab,
        mntPt,
        rmMntPtFromFstab,
        with context %}

{{rmMntPtFromFstab}}:
  file.replace:
    - name: {{mntFstab}}
    - pattern: {{mntPt}}/*
    - repl: /

{% endif %}  # hostnameIsCorrect
