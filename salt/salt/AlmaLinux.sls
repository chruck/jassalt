# salt/salt/AlmaLinux.sls

# Formula to specific to AlmaLinux:
# - enable PowerTools/CRB repo

{% set almaenablerepo = "dnf config-manager --set-enabled" %}
{{sls}} - Enable PowerTools/CRB repo for AlmaLinux:
  cmd.run:
    - name: {{almaenablerepo}} powertools 2>/dev/null || {{almaenablerepo}} crb
    - onlyif:
      - test "AlmaLinux" == {{grains.os}}
