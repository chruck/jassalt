# This file is part of OpenMediaVault.
#
# @license   http://www.gnu.org/licenses/gpl.html GPL Version 3
# @author    Volker Theile <volker.theile@openmediavault.org>
# @copyright Copyright (c) 2009-2022 Volker Theile
#
# OpenMediaVault is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# OpenMediaVault is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with OpenMediaVault. If not, see <http://www.gnu.org/licenses/>.

# OMV top file merged with jassalt top file by Jas:

base:
  '*':
    - userJas
    - passwd
    - luks
    - funtoo

# Clemson:
  iac,stack,zetta:
    - match: list
    - userEckard

# Ingram Micro:
  jastest,jas,salt-proto,tanium-proto,penguin:
    - match: list
    - userUseckj00

# VMware (SaltStack):
  cider,sysresccd:
    - match: list
    - funtoo.{{opts.id}}
  crystal:
#    - match: list
#    - userRocker
    - userJas

# Home:
  store:
    - omv
  tiger:
    - users.asciinema
  tiger,store:
    - match: list
    - users.btrfs

# All places, testing:
  'virtual:container':
    - match: grain
    - users.btrfs

  debian:
    - users.btrfs
