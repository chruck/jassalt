{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        downloadingThePortageTree,
        emergeSync,
        mntPt,
        with context %}
{%   from tpldir ~ "/headtail.jinja" import headtail with context %}

include:
  - {{downloadingThePortageTree}}

# Rebuilding gcc-5.4 in the future?  Maybe do that here.
{{sls}} - Update @world:
  cmd.run:
    - name: "/bin/chroot {{mntPt}} emerge -uDN @world {{headtail}}"
    - require:
      - {{emergeSync}}

{% endif %}  # hostnameIsCorrect
