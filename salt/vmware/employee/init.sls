# salt/vmware/employee/init.sls

{% from tpldir ~ "/map.jinja" import employeepkg with context %}
{% from "iscontainer.jinja" import is_container with context %}
{% if not is_container %}

include:
  - .noncontainer

{% endif %}  # container?

{{sls}} - Install employee pkgs:
  pkg.installed:
    - pkgs: {{employeepkg.list}}
    - onlyif: test "[]" != "{{employeepkg.list}}"
