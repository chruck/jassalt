{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        mntPt,
        makeConfFile,
        mountAsFuntoo,
        mountingFilesystems,
        numThreads,
        with context %}

include:
  - {{mountingFilesystems}}

{{sls}} - Set MAKEOPTS, USE, and CFLAGS in {{makeConfFile}}:
  file.managed:
    - name: {{makeConfFile}}
    - source: salt://gentoo/make.conf.jinja
    - template: jinja
    - defaults:
        numThreads: {{numThreads}}
    - require:
      - {{mountAsFuntoo}}

#{{sls}} - Set USE flag for consolekit:
#  portage_config.flags:
#    - name: ">=sys-auth/consolekit-1.0.1"
#    - use:
#      - policykit

{% endif %}  # hostnameIsCorrect
