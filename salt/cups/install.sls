# CUPS printer daemon installation state

{{sls}} - Install CUPS software:
  pkg.installed:
    - name: cups
