# salt/salt/ssh.sls

{% from "salt/map.jinja" import jassaltDir with context %}
{% from "pkgnames_map.jinja" import pkg_name with context %}

{% set saltRoster = "/etc/salt/roster" %}

include:
  - gentoo
  - salt.master

{{sls}} - Install salt-ssh pkg:
  pkg.installed:
    - name: {{pkg_name.salt_ssh}}
    - refresh: True

{{sls}} - Symlink for {{saltRoster}}:
  file.symlink:
    - name: {{saltRoster}}
    - target: {{jassaltDir}}/salt/salt/roster
    - force: True
    - require:
      - file: salt.master - Create /srv
