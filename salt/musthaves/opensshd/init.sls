{% if "unknown" != grains.init %}  # eg, Docker container
{%      from "musthaves/map.jinja" import musthaves with context %}

include:
  - musthaves

{{sls}} - Must-Haves, openssh daemon:
  pkg.installed:
    - name:  {{musthaves.opensshd.pkgName}}
    - refresh: True
#  - install_recommends: False


{{sls}} - Configure opensshd:
  file.append:
    - name: /etc/ssh/sshd_config
    - text:
      - X11Forwarding yes
    - require_in:
      - musthaves - Must-Haves
    - require:
      - {{sls}} - Must-Haves, openssh daemon

{{sls}} - Enable and start opensshd service:
  service.running:
    - name: {{musthaves.opensshd.svcName}}
    - enable: True
    - watch:
      - {{sls}} - Configure opensshd

{% endif %}  # unknown Init system
