{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        emergeSync,
        intelMicrocode,
        mntPt,
        with context %}
{%   from tpldir ~ "/headtail.jinja" import headtail with context %}

include:
  - .downloadingThePortageTree

{{sls}} - Install grub program:
  cmd.run:
    - name: "/bin/chroot {{mntPt}} emerge grub {{headtail}}"
    - require:
      - {{emergeSync}}

{{sls}} - Update /etc/boot.conf:
  file.managed:
    - name: {{mntPt}}/etc/boot.conf
    - source: salt://installFuntoo/boot.conf
    - require:
      - {{sls}} - Install grub program

{{intelMicrocode}}:
  cmd.run:
    - name: "/bin/chroot {{mntPt}} emerge intel-microcode iucode_tool {{headtail}}"
    - require:
      - {{emergeSync}}

{% endif %}  # hostnameIsCorrect
