{% from "musthaves/map.jinja" import musthaves with context %}

{% set jasHome = "/home/" ~ pillar['user'] ~ "/" %}
{% set gtSrc = jasHome ~ "/src/ghost-text-vim" %}
{% set jasBin = jasHome ~ "/bin/" %}
{% set programs = [
                   (jasBin ~ "ghost-text-server.tcl",
                    gtSrc ~ "/ghost-text-server.tcl"
                   ),
                  ] %}

include:
  - adduser
  - gentoo
  - .git

{{sls}} - Download GhostText Vim repo:
  git.latest:
    - name: https://github.com/falstro/ghost-text-vim.git
    - target: {{gtSrc}}
    - user: {{pillar['user']}}
    - require:
      - adduser - Create user 'jas'
      - musthaves.git - Install {{musthaves.gitpkg}} package
      - adduser - Create user '{{pillar.user}}'

{% for dest, src in programs %}
{{sls}} - Symlink {{src}} to {{dest}}:
  file.symlink:
    - name: {{dest}}
    - target: {{src}}
    - user: {{pillar['user']}}
    - group: {{pillar['user']}}
    - remote_name: {{pillar['user']}}
    - makedirs: True
    - require:
      - adduser - Create user 'jas'
      - {{sls}} - Download GhostText Vim repo
{% endfor %}
