# salt/firmware.sls

# Software that updates firmware.
# In particular, used for Dell laptops from VMware.

{{sls}} - Install firmware update daemon:
  pkg.installed:
    - pkgs:
      - fwupd
      # firmware-iwlwifi # on 'modern', possibly
      # hwdata # ?
      # fwupd-signed # available?
      # bluez-firmware # available?

      # partition:
      # wayland/xwayland
      # grub-gfxpayload-lists
      # ./debs/libfwupdplugin5_1.7.9-1~22.04.3_amd64.deb
      # ./debs/linux-modules-iwlwifi-oem-22.04d_6.5.0.1020.22_amd64.deb
      # ./debs/linux-modules-6.5.0-1020-oem_6.5.0-1020.21_amd64.deb
      # ./debs/linux-firmware_20220329.git681281e4-0ubuntu3.29_all.deb
      # ./debs/linux-image-oem-22.04d_6.5.0.1020.22_amd64.deb
      # ./debs/linux-modules-ipu6-oem-22.04d_6.5.0.1020.22_amd64.deb
      # ./debs/linux-modules-ivsc-oem-22.04d_6.5.0.1020.22_amd64.deb
      # ./debs/linux-modules-usbio-oem-22.04d_6.5.0.1020.22_amd64.deb
      # ./debs/linux-tools-oem-22.04d_6.5.0.1020.22_amd64.deb
      # ./debs/oem-config-debconf_22.04.20somerville2_all.deb
      # ./debs/oem-config-gtk_22.04.20somerville2_all.deb
      # ./debs/oem-config-remaster_22.04.20somerville2_all.deb
      # ./debs/oem-config_22.04.20somerville2_all.deb
      # ./debs/oem-fix-gfx-nvidia-ondemandmode_1.7_all.deb
      # ./debs/oem-jiayi-meta_22.04ubuntu3_all.deb
      # ./debs/oem-somerville-factory-meta_22.04ubuntu2_all.deb
      # ./debs/oem-somerville-factory-treecko-meta_22.04ubuntu4_all.deb
      # ./debs/oem-somerville-meta_22.04ubuntu2_all.deb
      # ./debs/oem-somerville-treecko-meta_22.04ubuntu4_all.deb
      # ./debs/screen-resolution-extra_0.18.2_all.deb
      # ./debs/wireless-regdb_2022.06.06-0ubuntu1~22.04.1_all.deb

      # somerville:
      # ./debs/main/dell-eula_1.11somerville1_all.deb
      # ./debs/oem-config-gtk_22.04.16somerville3~lp2017628.1_all.deb
      # ./debs/ubiquity-frontend-gtk_22.04.16somerville3~lp2017628.1_amd64.deb
      # ./debs/ubiquity_22.04.16somerville3~lp2017628.1_amd64.deb
      # ./debs/ubiquity-frontend-debconf_22.04.16somerville3~lp2017628.1_amd64.deb
      # ./debs/oem-config-debconf_22.04.16somerville3~lp2017628.1_all.deb
      # ./debs/oem-config-remaster_22.04.16somerville3~lp2017628.1_all.deb
      # ./debs/ubiquity-ubuntu-artwork_22.04.16somerville3~lp2017628.1_all.deb
      # ./debs/oem-config_22.04.16somerville3~lp2017628.1_all.deb
      # ./debs/dell-recovery-bootloader_1.68somerville13_all.deb
      # ./debs/dell-recovery-casper_1.68somerville13_all.deb
      # ./debs/dell-recovery_1.68somerville13_all.deb
      # ./debs/dell-recovery-bootloader_1.68somerville14_all.deb
      # ./debs/dell-recovery-casper_1.68somerville14_all.deb
      # ./debs/dell-recovery_1.68somerville14_all.deb
      # ./debs/oem-config-debconf_22.04.20somerville2_all.deb
      # ./debs/oem-config-gtk_22.04.20somerville2_all.deb
      # ./debs/oem-config-remaster_22.04.20somerville2_all.deb
      # ./debs/oem-config_22.04.20somerville2_all.deb
      # ./debs/oem-somerville-factory-meta_22.04ubuntu2_all.deb
      # ./debs/oem-somerville-factory-treecko-meta_22.04ubuntu4_all.deb
      # ./debs/oem-somerville-meta_22.04ubuntu2_all.deb
      # ./debs/oem-somerville-treecko-meta_22.04ubuntu4_all.deb
      # ./debs/ubiquity-frontend-debconf_22.04.20somerville2_amd64.deb
      # ./debs/ubiquity-frontend-gtk_22.04.20somerville2_amd64.deb
      # ./debs/ubiquity-ubuntu-artwork_22.04.20somerville2_all.deb
      # ./debs/ubiquity_22.04.20somerville2_amd64.deb


{{sls}} - Now, update the firmware manually as root:
  test.nop:
    - name: Use 'fwupdtool get-updates' and 'fwupdtool update'
