# salt/musthaves/googlechrome/RedHat.sls

{% if grains.os_family not in "RedHat", "Suse" %}

{{sls}} - Google Chrome browser, RedHat/SuSE:
  test.nop:
    - name: skipping, not RedHat or Suse ({{grains.os_family}})

{% else %}  # RedHat or Suse

include:
  - gpg

{{sls}} - Must-Haves for Desktop, Google Chrome repo, Redhat/SuSE:
  pkgrepo.managed:
    - name: GoogleChrome
    - humanname: GoogleChrome
    - baseurl: https://dl.google.com/linux/chrome/rpm/stable/x86_64
    - gpgkey: https://dl-ssl.google.com/linux/linux_signing_key.pub
    - gpgcheck: 1
    - gpgautoimport: True
    - require:
      - gpg - Install GPG

{{sls}} - Must-Haves for Desktop, Google Chrome:
  pkg.installed:
    - name: google-chrome-stable
    - refresh: True
#    - install_recommends: False
#    - uses: X
    - require:
      - {{sls}} - Must-Haves for Desktop, Google Chrome repo, Redhat/SuSE

{% endif %}  # RedHat or Suse
