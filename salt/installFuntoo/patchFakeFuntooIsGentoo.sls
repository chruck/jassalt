{% if True %}

{{sls}} - Not sure if needed anymore:
   test.nop:
     - name: No need to patch for Funtoo?

{% else %}  # True

{%   from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{%   if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{%   else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        installStage3,
        mntPt,
        with context %}

include:
  - .installingTheStage3Tarball

{{sls}} - Patch to fake salt into thinking Funtoo is Gentoo:
  file.replace:
    - name:  {{mntPt}}/etc/os-release
    - pattern: Funtoo
    - repl: Gentoo
    - require:
      - {{installStage3}}

{%   endif %}  # hostnameIsCorrect
{% endif %}    # True
