# salt/changehostname.sls

# State to change the hostname of the target, and restart salt-minion
# with the new hostname.  Requires the pillar '' on the commandline:
# sudo salt debian9minimal201906xx state.apply changehostname \
# pillar="{newhostname: 'blah'}"

{% from tpldir ~ "/iscontainer.jinja" import is_container with context %}

{% if pillar.newhostname is not defined %}

{{sls}} - Missing 'newhostname' on commandline:
  test.fail_without_changes:
    # - name: "Rerun with 'newhostname' pillar:\n        Name:\n        Name:     salt {{grains.id}} state.apply changehostname pillar=\"{'newhostname': '${newhostname}'}\"\n        Name:"
    - name: |
        Rerun with 'newhostname' pillar:
                Name:
                Name:     salt {{grains.id}} state.apply changehostname pillar="{'newhostname': '${newhostname}'}"
                Name:

{% else %}  # pillar.newhostname is defined:

# hostnamectl exists (systemd):
{%   if not salt.cmd.retcode("/bin/sh -c 'type hostnamectl'") %}

{{sls}} - Change hostname with hostnamectl:
  cmd.run:
    - name: hostnamectl set-hostname {{pillar.newhostname}}
    - onlyif: "! {{is_container|lower}}"

{%   else %}  # no hostnamectl:

{{sls}} - Change hostname in /etc/hostname:
  file.managed:
    - name: /etc/hostname
    - contents: {{pillar.newhostname}}

# TODO:  SuSE:  check if minimal install follows this format:
{{sls}} - Change hostname in /etc/hosts:
  file.replace:
    - name: /etc/hosts
    - pattern: {{grains.id}}
    - repl: {{pillar.newhostname}}
    - require:
      - {{sls}} - Change hostname in /etc/hostname

{%    if is_container %}

{{sls}} - Change in-memory hostname (container):
  test.succeed_with_changes:
    - name: hostname {{pillar.newhostname}}
    - require:
      - {{sls}} - Change hostname in /etc/hosts

{%    else %}  {# not a container #}

{{sls}} - Change in-memory hostname:
  cmd.run:
    - name: hostname {{pillar.newhostname}}
    - require:
      - {{sls}} - Change hostname in /etc/hosts

{%    endif %}  {# is_container? #}

{%  endif %}  # hostnamectl

{{sls}} - Change minion ID:
  file.managed:
    - name: /etc/salt/minion_id
    - contents: {{pillar.newhostname}}

{%   set pkidir = "/etc/salt/pki/minion" %}
{%   if "Ubuntu" == grains.os %}
{%     set pkidir = "/var/lib/salt/pki/minion" %}
{%   endif %}

{{sls}} - Delete the minion's keys:
  file.absent:
    - name: {{pkidir}}
    - require:
      - {{sls}} - Change minion ID

{{sls}} - Restart minion daemon:
  cmd.run:
    - name: 'sh -c "sleep 10; salt-call --local service.restart salt-minion" &'
    - require:
      - {{sls}} - Change minion ID
      - {{sls}} - Delete the minion's keys

{% endif %}  # pillar 'newhostname' defined
