# salt/installArchAtopFuntoo/setSaltUseEmerge.sls

# State to setup Salt minion to use 'emerge'.
# (replaced by identifyAsGentoo.sls)

{{sls}} - Force Salt to use 'emerge' for pkg:
  file.managed:
    - name: /etc/salt/minion.d/provider.conf
    - makedirs: True
    - contents:
      - "providers:"
      - "  pkg: ebuildpkg"
