{% from "pkgnames_map.jinja" import pkg_name with context %}

{{sls}} - Install GPG:
  pkg.installed:
    - name: {{pkg_name.gpg}}
