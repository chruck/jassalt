# salt/musthaves/firefox/install-Ubuntu.sls

{% if salt.pkg.version('snapd') %}

{{sls}} - Uninstall the Firefox snap:
  cmd.run:  # https://github.com/saltstack/salt/pull/56953 will add snap
    - name: snap remove firefox
    - onlyif: snap list |grep ^firefox

{{sls}} - Purge the Ubuntu transitional pkg:
  pkg.purged:
    - name: firefox
    - require:
      - {{sls}} - Uninstall the Firefox snap
    - require_in:
      - {{sls}} - Install Firefox from Mozilla's PPA

{% endif %}  # snapd installed

{{sls}} - Make Mozilla PPA have higher priority than Ubuntu transitional pkg:
  file.managed:
    - name: /etc/apt/preferences.d/mozilla-firefox
    - contents: |
        Package: firefox
        Pin: release o=LP-PPA-mozillateam
        Pin-Priority: 1001

{{sls}} - Ensure unattended-upgrades takes Mozilla PPA into account:
  file.managed:
    - name: /etc/apt/apt.conf.d/51unattended-upgrades-firefox
    - contents: |
        Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";
    - require:
      - {{sls}} - Make Mozilla PPA have higher priority than Ubuntu transitional pkg

{{sls}} - Add Mozilla's PPA:
#  pkgrepo.managed:
  cmd.run:
#    - ppa: mozillateam/ppa
    - name: add-apt-repository ppa:mozillateam/ppa -y
    - require:
      - {{sls}} - Ensure unattended-upgrades takes Mozilla PPA into account

# TODO
{{sls}} - Temporarily? add Mozilla's GPG key:
  cmd.run:
    - name: apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 9BDB3D89CE49EC21

{{sls}} - Install Firefox from Mozilla's PPA:
  pkg.installed:
    - name: firefox
    - refresh: True
    - require:
      - {{sls}} - Add Mozilla's PPA
    - watch:
      - {{sls}} - Add Mozilla's PPA

{{sls}} - Create AppArmor disable link for Firefox:
  file.symlink:
    - name: /etc/apparmor.d/disable/usr.bin.firefox
    - target: /etc/apparmor.d/usr.bin.firefox
    - makedirs: True
    - require:
      - {{sls}} - Install Firefox from Mozilla's PPA

{{sls}} - Disable AppArmor for Firefox to connect to KeepassXC:
  cmd.run:
    - name: apparmor_parser -R /etc/apparmor.d/usr.bin.firefox
    - onlyif: "apparmor_status --enabled && ( apparmor_status | grep -i firefox )"
    - require:
      - {{sls}} - Create AppArmor disable link for Firefox

# Run KeePassXC and enable browser integration (menu Tools ->
# Settings -> Browser Integration, tick "Enable browser integration" and
# "Firefox").

# Run Firefox, open settings of the KeePassXC extension ->
# Connected Databases -> Connect.
