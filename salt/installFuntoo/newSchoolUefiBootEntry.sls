{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        intelMicrocode,
        mntPt,
        sysMntPt,
        with context %}
{%   from tpldir ~ "/iscontainer.jinja" import is_container with context %}

include:
  - .installingABootloader

{%   set cmdrun = "cmd.run" %}

{%   if is_container %}

{%     set cmdrun = "test.succeed_with_changes" %}

{{sls}} - Running in container:
  test.succeed_with_changes:
    - name: Not installing grub inside a container

{%   endif %}  # container

# New School (UEFI) Boot Entry
# ---
#
# If you're using "new school" UEFI booting, run of the following sets
# of commands, depending on whether you are installing a 64-bit or
# 32-bit system. This will add GRUB as a UEFI boot entry.
#
# For x86-64bit systems:
#
# chroot # mount -o remount,rw /sys/firmware/efi/efivars

{{sls}} - Remount /sys/firmware/efi/efivars:
  mount.mounted:
    - name: {{sysMntPt}}/firmware/efi/efivars
    - device: efivarfs
    - fstype: efivarfs

# chroot # grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id="Funtoo Linux [GRUB]" --recheck /dev/sda
{{sls}} - Install grub as UEFI boot entry of {{pillar.funtoodisk}}:
  {{cmdrun}}:
#    - name: /bin/chroot {{mntPt}} grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id="Funtoo Linux [GRUB]" --recheck {{pillar.funtoodisk}}
    - name: /bin/chroot {{mntPt}} grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id="Funtoo Linux" --recheck {{pillar.funtoodisk}}
    - require:
      - installFuntoo.installingABootloader - Install grub program

# chroot # ego boot update
{{sls}} - Generate /boot/grub/grub.cfg:
  {{cmdrun}}:
    - name: /bin/chroot {{mntPt}} ego boot update
    - require:
      - installFuntoo.installingABootloader - Update /etc/boot.conf
      - {{intelMicrocode}}

# For x86-32bit systems:
#
# chroot # mount -o remount,rw /sys/firmware/efi/efivars
# chroot # grub-install --target=i386-efi --efi-directory=/boot --bootloader-id="Funtoo Linux [GRUB]" --recheck /dev/sda
# chroot # ego boot update
#
# First Boot, and in the future...
# ---
#
# OK -- you are almost ready to boot!
#
# You only need to run `grub-install` when you first install Funtoo
# Linux, but you need to re-run `ego boot update` every time you modify
# your `/etc/boot.conf`. When you emerge updated kernels, `ego boot
# update` will be run automatically as part of the install process. This
# will regenerate `/boot/grub/grub.cfg` so that you will have new
# kernels available in your GRUB boot menu upon your next reboot.
#
# Post reboot UEFI troubleshooting
# ---
#
# In case UEFI NVRAM boot entry is missing in BIOS and grub does not
# start you can try moving an already installed GRUB EFI executable to
# the default/fallback path.
#
# chroot # mv -v '/boot/EFI/Funtoo Linux [GRUB]' /boot/EFI/BOOT
# chroot # mv -v /boot/EFI/BOOT/grubx64.efi /boot/EFI/BOOT/BOOTX64.EFI

{% endif %}  # hostnameIsCorrect
