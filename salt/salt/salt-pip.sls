# salt/salt/salt-pip.sls

# State to install gcc and other packages needed for salt-pip to build
# pip wheels, then uninstall those packages.

{% if "onedir" == grains.get("package", "") %}

include:
  - .epel

{%   set devpkgs = [] %}
{%   for devpkg in "gcc", "patchelf" %}
{%     if not salt.pkg.version(devpkg) %}
{%       do devpkgs.append(devpkg) %}
{%     endif %}  # pkg not installed
{%   endfor %}   # list of dev pkgs

{{sls}} - Install dev pkgs for salt-pip:
  pkg.installed:
    - pkgs: {{devpkgs}}
    - order: 1

# TODO:  Downgrading pip due to a current bug in 3007
#        - 2024-09-01 08:50:58-04:00
{{sls}} - For Onedir Salt, downgrade pip:
  cmd.run:
    - name: salt-pip install pip==22
    - onlyif: test 22 != $(salt-pip -V |cut -d ' ' -f2 |cut -d. -f1 )

{# Fixed by enabling CRB repo
{{sls}} - Install patchelf via salt-pip:
  cmd.run:
    - name: salt-pip install patchelf
    - onlyif:
      - test "AlmaLinux" == {{grains.os}}
      - salt-pip
      - "! (salt-pip list |grep patchelf)"
#    - runas: salt
    - require:
      - {{sls}} - For Onedir Salt, downgrade pip
#}

{#
{{sls}} - Install pygit2:
  cmd.run:
    # pygit2 v1.14.1 per https://github.com/saltstack/salt/issues/66755
    # Hopefully to be fixed in 3007.2 .
    - name: salt-pip install pygit2==1.14.1
    - onlyif: "! (salt-pip list |grep pygit2)"
#    - runas: salt
    - require:
      - {{sls}} - Install dev pkgs for pygit2
#      - {{sls}} - Install patchelf via salt-pip
    - require_in:
      - {{sls}} - Remove dev pkgs for pygit2, now that they're not needed
#}

{{sls}} - Remove dev pkgs for pygit2, now that they're not needed:
  pkg.purged:
    - pkgs: {{devpkgs}}
    - order: last

{% else %}  # not OneDir version of Salt:

{#
{{sls}} - Install pygit2:
  pkg.installed:
    - pkg: python3-pygit2
#    - watch_in:
#      - {{mastersvcID}}
#}

{{sls}} - Not a OneDir package of Salt:
  test.nop

{% endif %}  # OneDir Salt?
