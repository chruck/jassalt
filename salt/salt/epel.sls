# salt/salt/epel.sls

# Formula to install EPEL (Extra Packages for Enterprise Linux) or its
# equivalent, CRB (CodeReady Builder) fka PowerTools.

{% if grains.os not in [ "RedHat", "AlmaLinux", "Amazon", "Rocky", ] %}

{{sls}} - EPEL/CRB formula only for RedHat family, not {{grains.os}}:
  test.nop

{% else %}  # not RedHat

{%     from "pkgnames_map.jinja" import pkg_name with context %}

{{sls}} - Install EPEL repo pkg:
  pkg.installed:
    - name: {{pkg_name.epel}}
    - onlyif:
#      - test "RedHat" == {{grains.os}}
#      - test "Fedora" != {{grains.os}}
      - test "Amazon" != {{grains.os}}
#      - test "AlmaLinux" == {{grains.os}}
    - order: 1

{% set enablerepo = "dnf config-manager --set-enabled" %}
{{sls}} - Enable PowerTools/CRB repo for Rocky and AlmaLinux:
  cmd.run:
    - name: {{enablerepo}} powertools 2>/dev/null || {{enablerepo}} crb
    - onlyif:
#      - test "AlmaLinux" == {{grains.os}}
#      - test "Rocky" == {{grains.os}}
      - '{{ ( grains.os in [ "AlmaLinux", "Rocky", ] ) | lower}}'
    - order: 1

{{sls}} - Enable Amazon Linux Extras:
  cmd.run:
    - name: amazon-linux-extras install epel -y
    - onlyif:
#      - test "Amazon" == {{grains.os}}
      - amazon-linux-extra |grep epel.*available
    - order: 1

{% endif %}  # RedHat?
