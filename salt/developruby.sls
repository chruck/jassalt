# salt/developruby.sls

# Salt state to setup a machine to be able to develop Ruby code.
# Originally for ThoroughCare.

{{sls}} - Install ruby-bundler:
  pkg.installed:
    - name:  ruby-bundler
