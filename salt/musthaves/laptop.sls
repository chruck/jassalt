# salt/musthaves/laptop.sls

{% from tpldir ~ "/map.jinja" import musthaves with context %}
{% from tpldir ~ "/vars.jinja" import musthaves4Laptop, with context %}

include:
  - .desktop

{{musthaves4Laptop}}:
  pkg.installed:
    - refresh: True
    - pkgs: {{musthaves.laptoplist}}

{# since 'laptopservices' is currently empty, commenting out:
{{sls}} - Enable and Start services:
  service.running:
    - names: {{musthaves.laptopservices}}
    - enable: True
{% from tpldir ~ "/iscontainer.jinja" import is_container with context %}
{% if is_container %}
    - onlyif: test "container" != {{grains.virtual}}
    - require:
      - {{musthaves4Laptop}}
#}
