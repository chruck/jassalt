# salt/installArchAtopFuntoo/createChroot.sls

# State to create a filesystem mountpoint on a Funtoo system for Arch
# scripts to chroot into.  This will later be the filesystem that Arch
# will be booted into.

{% set diskfile = "/builds/chruck/jassalt/btrfs.fakedisk" %}

include:
  - .identifyAsGentoo
  - .installBtrfs

#{{sls}} - Create btrfs subvolume:
#  btrfs.subvolume_created:
#    - name: /@archrootfs
#    #- name: @archrootfs
#    - device: {{diskfile}}
#    - require:
#      - installArchAtopFuntoo.installBtrfs - Make sure btrfs is installed

{{sls}} - Mount as Arch:
  mount.mounted:
    - name: /mnt
    #- device: {pillar.funtoodiskslash}}
    #- device: {{ salt.mount.get_device_from_path("/") }}
    - device: {{diskfile}}
    - fstype: btrfs
    #- pass_num: 1
    - mkmnt: True
    #- config: /mnt/etc/fstab
    #- require_in:
    #  - {rmMntPtFromFstab}}
    #- require:
    #  - {{sls}} - Create btrfs subvolume
