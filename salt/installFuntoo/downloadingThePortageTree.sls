{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}

{%   from tpldir ~ "/vars.jinja" import
        bindMountDev,
        chrootIntoFuntoo,
        emergeSync,
        mntPt,
        mountVirtFs,
        setTheTime,
        settingTheDate,
        with context %}
{%   from tpldir ~ "/headtail.jinja" import headtail with context %}

include:
  - {{chrootIntoFuntoo}}
  - {{mountVirtFs}}
  - {{settingTheDate}}

{{emergeSync}}:
  cmd.run:
#    - name: "/bin/chroot {{mntPt}} sh -c 'emerge --sync >/tmp/emerge-sync; rc=$?; tail -n50 /tmp/emerge-sync; exit $rc'"
    - name: "/bin/chroot {{mntPt}} ego sync {{headtail}}"
    - require:
      - {{chrootIntoFuntoo}} - Ping in chroot of {{mntPt}}
      - {{bindMountDev}}
      - {{setTheTime}}

{% endif %}  # hostnameIsCorrect
