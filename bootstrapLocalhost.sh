#!/bin/bash

##
# @file bootstrapLocalhost.sh
# @author Jas Eckard <jas@eckard.com>
#
# @section LICENSE
#
# GPL license
#
# @section DESCRIPTION
#
# Script to use system `localhost' as a bootstrapped system to get
# everything going.  Will:
# - create an /etc/hosts entry for 'salt' to localhost
# - upgrade all packages on the system
# - accept its salt-key and become its minion
#
# Assumptions:
# * localhost is a bare Bodhi Linux system
#   * therefore, package manager is apt
# * localhost will be a minion to itself first, until another master is
#   built
#
# Return codes:  1 = bad parameter(s)
#                2 = required parameter missing

#readonly localDir="$(cd ${BASH_SOURCE[0]%/*} && pwd -P && cd - >/dev/null)"

# To get some debug output call this file like:
# DEBUG=true ./bootstrapLocalhost.sh ...

echoStderr() { echo "$@" 1>&2; }

if [[ '' == "${DEBUG}" ]]; then
        # Make `debug' a no-op
        debug() { :; }
else
        debug() {
                echoStderr -n "DEBUG:  "
                echoStderr "$@"
        }
fi

# Usages:
#echo “regular stdout output”
#echoStderr “regular stderr output”
#debug “stderr output only seen when DEBUG set”

declare rc=0
#declare optionA=0
#declare optionB=bogus
#declare REQUIRED
#declare OPTIONAL=bogus
declare MASTER="not_master"
#readonly pi="3.1415"
declare SUDO=sudo
declare IS_CONTAINER=false

MASTER="$(hostname)"

Usage() {
        cat 1>&2 <<EOFUsage

Usage:  $0 [OPTION]... #REQUIRED [MASTER]

#  -a, --optionA         Description
#  -b, --optionB VAL     Desc that takes VAL
  -h, -?, --help        Display this help and exit

EOFUsage
        exit ${rc}
} # Usage()

parseOptions() {
        #debug "Original args:  $@"
        debug "Original args:  $*"

        local shortopts="h?"
        local longopts="help,"
        #        shortopts+="a"; longopts+="optionA,"
        #        shortopts+="b:"; longopts+="optionB:,"

        debug "shortopts=${shortopts} longopts=${longopts}"

        ARGS=$(getopt -o "${shortopts}" -l "${longopts}" -n "getopt.sh" -- "$@")

        getoptRc=$?
        if [[ 0 -ne "${getoptRc}" ]]; then
                echoStderr "ERROR:  getopt called incorrectly, rc=${getoptRc}"
                rc=1
                exit ${rc}
                # Alternatively, ($rc gets passed):
                #Usage
        fi

        eval set -- "${ARGS}"

        while true; do
                param=$1
                case "${param}" in
                #                        -a|--optionA)
                #                                debug "-a used"
                #                                optionA=1
                #                                shift
                #                                ;;
                #                        -b|--optionB)
                #                                shift
                #                                if [[ -n "$1" ]]; then
                #                                        debug "-b used: $1";
                #                                        optionB=$1
                #                                        shift;
                #                                fi
                #                                ;;
                -h | -\? | --help)
                        debug "Asked for help"
                        Usage
                        ;;
                --)
                        shift
                        break
                        ;;
                *)
                        echoStderr "ERROR:  Unimplemented option: ${param}"
                        break
                        ;;
                esac
        done

        #debug "New args:  $@"
        debug "New args:  $*"

        #        # For 1 required and one optional argument
        #        if [[ 1 != ${#@} && 2 != ${#@} ]]; then
        #                rc=2
        #                echoStderr "ERROR:  REQUIRED required."
        #        fi

        #        REQUIRED=$1
        #        OPTIONAL=$2
        MASTER=${1:-${MASTER}}

        #        if [[ 'bogus' == "${optionB}" ]]; then
        #                rc=2
        #                echoStderr "ERROR:  --optionB field required."
        #        fi

        if [[ 0 != "${rc}" ]]; then
                Usage
        fi
} # parseOptions()

# TODO:  Verify the parameters passed are valid
verifyParams() {
        :
        #       if [[ "${optionB}" =~ /reg[ex]/ ]]; then
        #               echoStderr "ERROR:  --optionB should be of the form: reg[ex]"
        #               rc=2
        #       fi
} # verifyParams()

isSudoNeeded() {
        if [[ 0 -eq ${UID} ]]; then
                SUDO=""
        fi
} # isSudoNeeded()

#        while read line; do
#                echoStderr "WARNING:  I do not like ${line}"
#        done < <(grep "^${softwarebom}" ${steutabFName})

setupIptables() {
        # IPTables on a bare system is blank, so this is not needed:
        # (but we want to enable it anyway for this master)

        local IPTABLES="$(type -P iptables)"

        if [[ "" == "${IPTABLES}" ]]; then
                echo "==== iptables not installed, skipping ===="
                return
        fi

        ${SUDO} "${IPTABLES}" -A INPUT -m state --state new -m tcp -p tcp \
                --dport 4505 -j ACCEPT
        ${SUDO} "${IPTABLES}" -A INPUT -m state --state new -m tcp -p tcp \
                --dport 4506 -j ACCEPT
} # setupIptables()

updateOS() {
        echo "== First, make sure OS is up-to-date =="
        ${SUDO} apt update && ${SUDO} apt -y upgrade
} # updateOS()

installSalt() {
        case "${1}" in
        apt)
                echo "== Install salt-master package =="
                if ${SUDO} apt install -y git salt-master salt-minion; then
                        return
                fi
                ;&

        bootstrap)
                # If, instead, I want the latest salt (I don't,
                # since Bodhi is now installing 2014.7.2 of salt)
                echo "== Uninstalling any previous salt packages =="
                ${SUDO} apt purge -y salt-master salt-minion salt-ssh
                        salt-cloud salt-common salt-syndic

                echo "== Install curl and git to bootstrap SaltStack =="
                ${SUDO} apt install -y git curl

                echo "== Download and execute SaltStack Bootstrap =="
                curl -L http://bootstrap.saltproject.io | ${SUDO} sh -xs --
                ;;

        *)
                echo "== installSalt() needs an argument, exiting =="
                exit 2
                ;;
        esac
} # installSalt()

masterIsLocal() {
        echo "== Create hostname 'salt' as alias to localhost =="

        if mount |grep /etc/hosts; then
                echo "==== Probably a container:  skipping ===="
                IS_CONTAINER=true
                return
        fi

        ${SUDO} sed -i "s/127.0.0.1.*/& salt/" /etc/hosts
} # masterIsLocal()

startDaemons() {
        echo "== Starting daemons =="

        if ${IS_CONTAINER}; then
                salt-master &
                salt-minion &
                return
        fi

        ${SUDO} service salt-master restart
        ${SUDO} service salt-minion restart
} # startDaemons()

acceptSaltkey() {
        echo "== Accepting ${MASTER} =="
        ${SUDO} salt-key -ya "${MASTER}*"
} # acceptSaltkey()

checkoutStates() {
        echo "== Checking out salt states =="
        pushd /srv || exit 3
        ${SUDO} git clone https://gitlab.com/chruck/jassalt.git
        ${SUDO} ln -fs jassalt/salt .
        ${SUDO} ln -fs jassalt/pillar .
        popd || exit 3
} # checkoutStates()

deploy() {
        echo "== Now deploy to ${MASTER} =="
        ${SUDO} salt "${MASTER}" state.highstate test=t
        ${SUDO} salt "${MASTER}" state.highstate
} # deploy()

parseOptions "$@"
verifyParams
isSudoNeeded
setupIptables
updateOS
installSalt apt
#installSalt bootstrap
masterIsLocal
startDaemons
acceptSaltkey
checkoutStates
deploy
