funtoodisk: /builds/chruck/jassalt/funtoodisk
# https://build.funtoo.org/1.4-release-std/x86-64bit/generic_64/2021-10-09/stage3-generic_64-1.4-release-std-2021-10-09.tar.xz
funtoocpu: generic_64
# from /sys/devices/cpu/caps/pmu_name:
funtoocpusubarch: generic_64
funtoorelease: 1.4
funtooreleasedateofstage3: 2021-10-09
