include:
  - gpg

{% set distro = ( grains.os_family|lower ) %}
{% if ( grains.os|lower ) in [
                                "amazon"
                                "arista"
                                "fedora"
                                "juniper"
                                "photon"
                                "solaris"
                                "ubuntu"
                        ] %}
{%   set distro = ( grains.os|lower ) %}
{% endif %}  # Distro one-offs

# TODO:  Replace with broadcom.com:
{% set repoUrl = "https://repo.saltproject.io/salt/py3/"
                    ~ distro ~ "/"
                    ~ grains.osmajorrelease ~ "/"
                    ~ grains.osarch %}

{% if "Debian" == grains.os_family %}

{{sls}} - Install Salt Project repository:
  pkgrepo.managed:
    - humanname: "Salt Project repo"
    # name: "deb [arch=amd64] {{repoUrl}}/latest {{grains.oscodename}} main"
    # name: "deb [signed-by=/etc/apt/keyrings/salt-archive-keyring.gpg arch=amd64] {{repoUrl}}/latest {{grains.oscodename}} main"
    # name: "deb [arch=amd64 signed-by=/usr/share/keyrings/salt-archive-keyring.gpg] {{repoUrl}}/latest {{grains.oscodename}} main"
    - name: "deb [signed-by=/usr/share/keyrings/salt-archive-keyring.gpg] {{repoUrl}}/latest {{grains.oscodename}} main"
    # From https://docs.saltproject.io/en/latest/ref/states/all/salt.states.pkgrepo.html
    # name: "deb [signed-by=/etc/apt/keyrings/salt-archive-keyring.gpg arch=amd64] https://repo.saltproject.io/py3/ubuntu/18.04/amd64/latest bionic main"
    # file: /etc/apt/sources.list.d/salt.list
    - file: /usr/share/keyrings/salt-archive-keyring.gpg
    - key_url: {{repoUrl}}/SALT-PROJECT-GPG-PUBKEY-2023.gpg
    - gpgcheck: 1
    - aptkey: False
    - require:
      - gpg - Install GPG

{% elif "RedHat" == grains.os_family %}

{%   if "Fedora" == grains.os %}

{{sls}} - Install Salt Project repository:
  test.nop:
    - name: No repo needed for Fedora?
    # Well, based on https://repo.saltproject.io/salt/py3/fedora ,
    # maybe there is.  Seems before_script (calling bootstrap) didn't
    # add that, though?

{%   else %}  # non-Fedora RH:

{{sls}} - Install Salt Project repository:
  pkgrepo.managed:
    - name: salt
    - humanname: "Salt Project repo"
    - baseurl: {{repoUrl}}/latest
    - gpgcheck: 1
    - gpgkey: {{repoUrl}}/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub

{%   endif %}  # Fedora
{% endif %}    # Distro
