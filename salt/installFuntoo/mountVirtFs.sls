{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,
        stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        bindMountDev,
        bindMountProc,
        bindMountSys,
        fixFstab,
        mountAsFuntoo,
        mountingFilesystems,
        mntFstab,
        mntPt,
        rmMntPtFromFstab,
        with context %}

include:
  - {{mountingFilesystems}}
  - {{fixFstab}}

{{bindMountProc}}:
  mount.mounted:
    - name: {{mntPt}}/proc
    - device: proc
    - fstype: proc
    - mkmnt: True
    - config: {{mntFstab}}
    - require:
      - {{mountAsFuntoo}}
    - require_in:
      - {{rmMntPtFromFstab}}

{{bindMountSys}}:
  mount.mounted:
    - name: {{mntPt}}/sys
    - device: /sys
    - fstype: sysfs
    - mkmnt: True
    - opts: rbind
    - config: {{mntFstab}}
    - require:
      - {{mountAsFuntoo}}
    - require_in:
      - {{rmMntPtFromFstab}}

{{bindMountDev}}:
  mount.mounted:
    - name: {{mntPt}}/dev
    - device: /dev
    - fstype: devtmpfs
    - mkmnt: True
    - opts: rbind
    - config: {{mntFstab}}
    - require:
      - {{mountAsFuntoo}}
    - require_in:
      - {{rmMntPtFromFstab}}

{% endif %}  # hostnameIsCorrect
