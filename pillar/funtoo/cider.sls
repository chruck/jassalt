funtoodisk: /dev/sda2
# https://build.funtoo.org/1.4-release-std/x86-64bit/intel64-haswell/2021-10-10/stage3-intel64-haswell-1.4-release-std-2021-10-10.tar.xz
funtoocpu: intel64
# from /sys/devices/cpu/caps/pmu_name:
funtoocpusubarch: haswell
funtoorelease: 1.4
funtooreleasedateofstage3: 2021-10-10
