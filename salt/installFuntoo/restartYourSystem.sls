{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        mntPt,
        mntFstab,
        umountSda,
        installSalt,
        setYourRootPassword,
        configuringYourNetworkWiFi,
        newSchoolUefiBootEntry,
        configurationFilesfstab,
        with context %}
#        oldSchoolBiosMbr,

include:
  - {{umountSda}}
  - {{installSalt}}
  - {{setYourRootPassword}}
  - {{configuringYourNetworkWiFi}}
{#  - {{oldSchoolBiosMbr}} #}
  - {{newSchoolUefiBootEntry}}
  - {{configurationFilesfstab}}

{{sls}} - Reboot:
  # cmd.run:
  module.run:
    # - name: reboot
    - system.reboot:
    - require:
      - {{umountSda}} - Unmount {{mntPt}}
      - {{installSalt}} - Start salt-minion at startup
      - {{setYourRootPassword}} - Set root's password
      - {{configuringYourNetworkWiFi}} - Start NetworkManager at startup
{#      - {{oldSchoolBiosMbr}} - Install grub to MBR of {{pillar.funtoodisk}} #}
      - {{newSchoolUefiBootEntry}} - Install grub as UEFI boot entry of {{pillar.funtoodisk}}
      - {{configurationFilesfstab}} - Add / to {{mntFstab}}

{% endif %}  # hostnameIsCorrect
