# salt/gentoo/init.sls

{% if "Gentoo" != grains.os %}

{{sls}} - Not Gentoo - {{grains.os}}:
  test.nop

{% else %}  # Gentoo

include:
  - .useflags
  - .software

{% endif %}  # Gentoo
