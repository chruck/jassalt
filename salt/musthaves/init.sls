# salt/musthaves/init.sls

{% from tpldir ~ "/map.jinja" import musthaves with context %}

include:
  - gentoo
  - .aur
  - .mlocate
  - .opensshd
#  - .salt-minion
  - .screen
  - .vim

{{sls}} - Must-Haves:
  pkg.installed:
    - refresh: True
#    - install_recommends: False
# Even if using EPEL, OracleLinux and AmazonLinux don't have PowerTool repo:
{% if grains.os not in ["OEL", "Amazon"] %}
    # Even if using EPEL, RedHat-based distros need 'PowerTools' enabled:
    - enablerepo: powertools
{% endif %}  # OracleLinux or AmazonLinux?
    - pkgs: {{musthaves.pkglist}}
