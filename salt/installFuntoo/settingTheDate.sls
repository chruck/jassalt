{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,
        stateNotInstalling,
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        setTheTime,
        with context %}

{{setTheTime}}:
  cmd.run:
    - name: ntpd -gq

{% endif %}  # hostnameIsCorrect
