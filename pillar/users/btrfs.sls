users:
  btrfs:
    fullname: "BTRFS remote backup user"
#    shell: /bin/btrfs
    sudo_rules:
      - "ALL = (root) NOPASSWD: /bin/btrfs"
    ssh_keys:
      privkey: salt://keys/btrfs.sshkey
    ssh_auth_sources:
      - salt://keys/btrfs.sshkey.pub
    ssh_known_hosts:
      store:
        port: 22
#        fingerprint: # 16 hex pairs separated by ':'
#        fingerprint: anYl7rOKyKoAl5jkJX9HgjKZgYblOn7mQnhX9B8CDdk
        fingerprint_hash_type: sha256
        enc: ssh-rsa
        key: >
          AAAAB3NzaC1yc2EAAAADAQABAAABAQDbuMobN1GfEBGtIKZMvNCTFjGy4WA
          ZvUrzOM3dxRnbxVOMhhUQv1CVPo6wHQHRsBhKfQrT41cQ5VaNTCQeFv/Dnf
          9AUGGAchahjw9TppD7Zgw8zbzKS8rpT5F7MLt/rwr9N2wTMU9KioAUJYE1N
          GNPImj6qQfB7fT/ek0fjssB4PMFhl68v/ahlK2rbLboPxnJhwuDxibBzBIE
          Z+XUwl9LEVwwtVFkf2qyz1914NmIIO3sXvNpRufcestQOYkGWD4tuRGkACc
          rdOs6e2nh61iWEaq/RbvYEzKy5xC/etRt1LWBP1eE/xdo23KzVOYsVqpZNo
          Tv+2/+jnUgUd/13VnR
        hash_known_hosts: true
