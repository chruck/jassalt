# salt.subdirs:  Create the subdirectories in /etc/salt

{% set masterDir = "/etc/salt/master.d" %}
{% set minionDir = "/etc/salt/minion.d" %}

{{sls}} - Create salt subdirectories:
  file.directory:
    - names:
      - {{masterDir}}
      - {{minionDir}}
    - makedirs: True
