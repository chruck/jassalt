# jassalt

Jas' Salt Repository

## Hints

```sh
wget https://gitlab.com/chruck/jassalt/-/raw/master/mkLocalhostSaltmaster.sh -O - | bash -
wget https://gitlab.com/chruck/jassalt/-/raw/master/bootstrapLocalhost.sh -O - | bash -
# WAS:  curl -L https://bootstrap.saltproject.io | sudo sh
curl -L https://raw.githubusercontent.com/saltstack/salt-bootstrap/refs/heads/develop/bootstrap-salt.sh | sudo sh
# WAS:  wget -O - https://bootstrap.saltproject.io | sudo sh
wget -O - https://raw.githubusercontent.com/saltstack/salt-bootstrap/refs/heads/develop/bootstrap-salt.sh | sudo sh
```
