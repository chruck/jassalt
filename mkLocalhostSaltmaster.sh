#!/bin/bash

# mkLocalhostSaltmaster.sh
# Script to make localhost (the host it is run on) a salt master
# without using git to download the whole 'jassalt' tree.  Requires
# 'wget' and 'sudo', and current user to be able to sudo as root.

for program in wget salt-call sudo; do
        if [[ -z $(type -P ${program}) ]]; then
                echo "${program} is not installed, but needed for this script."
                exit 7
        fi
done

wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/adduser.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/pkgnames_map.jinja

mkdir -p bashrc
pushd bashrc || exit 5
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/bashrc/init.sls
wget -N https://gitlab.com/chruck/dot.bashrc.jas/raw/master/.bashrc.jas
popd || exit 6

mkdir -p gentoo
pushd gentoo || exit 7
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/gentoo/init.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/gentoo/sudo.sls
popd || exit 8

mkdir -p musthaves
pushd musthaves || exit 3
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/musthaves/git.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/musthaves/map.jinja
popd || exit 4

mkdir -p salt
pushd salt || exit 1
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/at.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/bashcomplete.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/epel.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/file_ignore.conf
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/gitfs.conf
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/gitfs.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/jassalt.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/map.jinja
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/master.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/mastersvc.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/minion.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/minionsvc.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/salt-pip.sls
wget -N https://gitlab.com/chruck/jassalt/raw/master/salt/salt/subdirs.sls
popd || exit 2

#$(type -P sudo) salt-call --local --file-root=. state.sls salt.master -ldebug
$(type -P sudo) salt-call --local --file-root=. state.sls salt.master
