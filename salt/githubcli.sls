# salt/githubcli.sls

# Install the GitHub commandline `gh` package for the `gh` command.
# Probably behind the latest version.

{{sls}} - Install GitHub CLI pkg:
  pkg.installed:
    - name: gh
