{% from tpldir ~ "/map.jinja" import vim with context %}

include:
  - gentoo

{% if "Arch" == grains.os_family %}

# TODO:  Arch pkg 'gvim' (for musthaves.desktop) conflicts with 'vim', so
#        'vim' needs to be purged (currently vim.pkglist is empty).

{{sls}} - Remove original ex/vi (Arch):
  pkg.purged:
    - name: vi

{{sls}} - Symlink vi to vim:
  file.symlink:
    - name: /bin/vi
    - target: /usr/bin/vim

{% endif %}  # Arch

{{sls}} - Install Vim packages:
  pkg.installed:
    - refresh: True
    - install_recommends: False
    - pkgs: {{vim.pkglist}}

{% if vim.vimaddonmanager is defined %}

{{sls}} - Install {{vim.vimaddonmanager}}:
  pkg.installed:
    - name: {{vim.vimaddonmanager}}
    - refresh: True
    - install_recommends: False

{{sls}} - Enable Vim addons:
  cmd.wait:
    - name: vim-addons install -w info jinja matchit omnicppcomplete xmledit
    - watch:
      - pkg: {{vim.vimaddonmanager}}

{% endif %}
