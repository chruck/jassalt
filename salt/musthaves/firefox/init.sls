# salt/musthaves/firefox/init.sls

{% if "Ubuntu" == grains.os %}

include:
  - .install-Ubuntu

{% else %}  # not Ubuntu

{{sls}} - Firefox already installed via 'musthaves.desktopbrowserlist':
  test.nop

{% endif %}  # Ubuntu?
