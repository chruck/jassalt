# salt/bashcomplete.sls

{% set group = pillar.user %}
{% set saltgroup = salt.group.info("salt") %}
{% if saltgroup %}
{%   set group = "salt" %}
{% else %}  # since group "salt" doesn't exist, give it to Jas

include:
  - adduser

{% endif %}  # group salt exists?

# TODO:  Reconsider this state altogether, now that onedir salt runs
#        as user 'salt', these directories should be owned by group
#        'salt' with user 'jas' a member of group 'salt' (already the
#        case?).

{{sls}} - Allow user to tab-complete minion names:
  file.directory:
    - names:
      - /etc/salt/pki
      - /etc/salt/pki/master
      - /etc/salt/pki/master/minions
    - dir_mode: '0750'
    - group: {{group}}
{% if not saltgroup %}
    - require:
      - adduser - Create user '{{pillar.user}}'
{% endif %}  # no 'salt' group?
