{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        bindMountDev,
        bindMountProc,
        bindMountSys,
        installStage3,
        mntPt,
        stage3url,
        with context %}
{%   from tpldir ~ "/headtail.jinja" import headtail with context %}
# CPU architecture is /sys/devices/cpu/caps/pmu_name

include:
  - .mountVirtFs

# Hoping this has been fixed:
# Prefer cmd.run below, since trim_output doesn't work and output
# still scrolls off the screen
{{installStage3}}:
  archive.extracted:
    - name: {{mntPt}}
    - source: {{stage3url}}
#    - source_hash: {{stage3url}}.hash.txt  # needs release
    - skip_verify: True  # since hash not generic
    - options: p
    - trim_output: True
    - if_missing: /etc/gentoo-release
    - require:
      - {{bindMountDev}}
      - {{bindMountProc}}
      - {{bindMountSys}}

#{{installStage3}}:
#  cmd.run:
#    - name: "cd {{mntPt}} ; wget -nv -O - {{stage3url}} | tar xvpJf - {{headtail}}"
#    - require:
#      - {{bindMountDev}}
#      - {{bindMountProc}}
#      - {{bindMountSys}}

{% endif %}  # hostnameIsCorrect
