# salt/musthaves/googlechrome/Arch.sls

{% if "Arch" != grains.os_family %}

{{sls}} - Google Chrome browser:
  test.nop:
    - name: skipping, not Arch ({{grains.os_family}})

{% else %}  # Arch

{{sls}} - Must-Haves for Desktop, Google Chrome, Arch:
  cmd.run:
    - name: pamac build --no-confirm google-chrome
    - onlyif:
      # Use `pamac search -a` to prime pamac database with AUR.
      # `pamac` return 0 even if not found, but since not found gives no
      # output, test for empty string:
      - test "$(pamac search -a google-chrome)"
      - "! test -x /usr/bin/google-chrome-stable"

{% endif %}  # Arch
