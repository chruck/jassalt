user:
  jas
# below fails in adduser due to ':', where pillar.user is in ID:
#  jas:
#    groups:
#    optional_groups:
#      - adm
#      - plugdev
#      - sudo
#      - users
#      - wheel
#    - issudo: True
