Using `btrfs send/receive` for backups
===

Thoughts on how to approach using the `btrfs` subcommands `send`
and `receive` for automated backups of laptops, desktops, etc
---

Requirements:

- [ ] both systems must have `btrfs` filesystem to be backed up
- [ ] will use `ssh` to issue `btrfs` commands
- [ ] backup server should initiate the `ssh` session, therefore:
  - [ ] sets up `btrfs receive` on itself
  - [ ] has remote issue `btrfs send`
- [ ] user `btrfs`:
  - [ ] exists on target
  - [ ] has full `sudo`
    - [ ] (OR can do full `btrfs send`?)
  - [ ] has ssh pub key of backup server's user (?)
- [ ] server has list of clients with filesystems that are btrfs
- [ ] server can `ssh` to the list of clients
- [ ] server has cronjob to `btrfs receive` regularly
