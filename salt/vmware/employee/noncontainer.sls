# salt/vmware/employee/noncontainer.sls

{% from "iscontainer.jinja" import is_container with context %}

{% if is_container %}

{{sls}} - Inside a container:
  test.show_notification:
    - name: In a container
    - text: This state is meant to be for physical or virtual machines.

{% else %}  # not a container:

{{sls}} - Install Vagrant:
  pkg.installed:
    - name: vagrant

# Find the latest version of vagrant-vmware-utility by spidering:
{#   loop through the versions on the site #}
{%   set url = "https://releases.hashicorp.com/vagrant-vmware-utility/" %}
{%   set html = url | http_query  %}
{%   set latestver = {"ver": "0.0"} %}

{%   for version in html["body"].split('"') | unique %}
{%     set verlist = version | regex_search("/vagrant-vmware-utility/([0-9\.]*)") %}
{%     if verlist and salt.pkg.version_cmp(verlist[0], latestver["ver"]) > 0 %}
{%       do latestver.update({"ver": verlist[0]}) %}
{%     endif %}
{%   endfor %} # versions

{%   set ver = latestver["ver"] %}
{%   set pkgtype = "deb" %}  # Others are zip, dmg, msi, and rpm

{#   loop through the pkgs in that version #}
{%   set url = url ~ ver %}
{%   set html = url | http_query  %}
{%   set fullurl = {"url": "nourl"} %}

{%   for quotedstr in html["body"].split('"') | unique %}
{%     set pkglisting = quotedstr | regex_search("(" ~ url ~ ".*" ~
                pkgtype ~ ")") %}
{%     if pkglisting %}
{%       do fullurl.update({"url": pkglisting[0]}) %}
{%     endif %}
{%   endfor %} # quotedstr

{%   set url = fullurl["url"] %}

# https://www.vagrantup.com/docs/providers/vmware/vagrant-vmware-utility
# says to use zip, but I prefer deb:
{{sls}} - Install Vagrant VMware Desktop Utility:
  pkg.installed:
#    - name: vagrant-vmware-utility
    - sources:
      - vagrant-vmware-utility: {{url}}
#    - skip_verify: True
    - require:
      - {{sls}} - Install Vagrant

{%   set cmds = [
                "/opt/vagrant-vmware-desktop/bin/vagrant-vmware-utility certificate generate",
              ] %}
# WAS:  "/opt/vagrant-vmware-desktop/bin/vagrant-vmware-utility service install",

{{sls}} - Setup Vagrant VMware Desktop Utility:
  cmd.run:
    - names: {{cmds}}
    - onlyif: "! test -f /opt/vagrant-vmware-desktop/certificates/vagrant-utility.crt"
    - require:
      - {{sls}} - Install Vagrant
      - {{sls}} - Install Vagrant VMware Desktop Utility

{%   set installed = salt.cmd.shell("vagrant plugin list "
                "|cut -f1 -d\  ", runas=pillar.user).split() %}
{%   set plugins = [
                "vagrant-vmware-desktop",
                "winrm",
                "winrm-fs",
                "winrm-elevated",
              ] %}
{%   set missing = plugins | difference(installed) %}

{{sls}} - Install Vagrant plugins:
  cmd.run:
    - name: vagrant plugin install {{missing | join(" ")}}
    - runas: {{pillar.user}}
    - onlyif: test "[]" != "{{missing}}"
    - require:
      - {{sls}} - Install Vagrant
      - {{sls}} - Setup Vagrant VMware Desktop Utility

{{sls}} - Start Vagrant VMware Desktop Utility:
  service.running:
    - name: vagrant-vmware-utility
    - require:
      - {{sls}} - Install Vagrant VMware Desktop Utility
      - {{sls}} - Install Vagrant plugins
    - watch:
      - {{sls}} - Install Vagrant plugins
      - {{sls}} - Setup Vagrant VMware Desktop Utility

{{sls}} - Install podman:
  pkg.installed:
    - pkgs:
      - podman
{%   if "RedHat" == grains.os_family %}
      - podman-docker
{%   endif %}

{% endif %}  # container
