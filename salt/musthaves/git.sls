{% if "Windows" == grains.os_family %}

include:
  - .git-windows

{% else %}  # not Windows:

# Separated out to satisfy another state
{%   from tpldir ~ "/map.jinja" import musthaves with context %}

include:
  - gentoo

{{sls}} - Install {{musthaves.gitpkg}} package:
  pkg.installed:
    - name: {{musthaves.gitpkg}}
    - refresh: True
    - install_recommends: False

{# Not sure what this is for, the only ref in map.jinja is empty?
{%   if musthaves.gitpkgs is defined %}

{{sls}} - Install git packages:
  pkg.installed:
    - names: {{musthaves.gitpkgs}}
    - refresh: True
    - install_recommends: False

{%   endif %}     # git packages
#}

{% endif %}       # Windows
