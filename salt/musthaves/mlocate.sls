include:
  - musthaves

# First is for 'locate', second is 'mlocate':
{% for locatefile in "/etc/cron.daily/locate", "/etc/updatedb.conf" %}

{{sls}} - {{locatefile}} shouldn't check for /.snapshots and /home/.snapshots:
  file.replace:
    - name: {{locatefile}}
    - pattern: 'PRUNEPATHS="'
    - repl: 'PRUNEPATHS="/.snapshots /home/.snapshots '
    - ignore_if_missing: True
    - onlyif:
      - '! ( egrep "PRUNEPATHS=(.*\.snapshots){2}" {{locatefile}} 2>/dev/null)'
    - require:
      - musthaves - Must-Haves

{% endfor %}  # locate files
