# salt/musthaves/googlechrome/Gentoo.sls

{% if "Gentoo" != grains.os_family %}

{{sls}} - Google Chrome browser:
  test.nop:
    - name: skipping, not Gentoo ({{grains.os_family}})

{% else %}  # Gentoo

include:
  - gentoo

{{sls}} - Must-Haves for Desktop, Google Chrome, Gentoo:
  pkg.installed:
    - name: www-client/google-chrome
    - refresh: True
#    - install_recommends: False
#    - uses: X
    - require:
      - gentoo.useflags - Set USE flags for google-chrome
      - gentoo.useflags - Set USE flags for libpng
      - gentoo.useflags - Set license flag for google-chrome

{% endif %}  # Gentoo
