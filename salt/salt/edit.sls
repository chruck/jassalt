{% from "musthaves/map.jinja" import musthaves with context %}
{% from "salt/map.jinja" import etcVim with context %}

{% set srcDir = "/usr/src/salt-vim" %}

include:
  - musthaves.git
  - gentoo

{{sls}} - Vim files for editing Saltstack files:
  git.latest:
    - name: https://github.com/saltstack/salt-vim.git
    - target: {{srcDir}}
    - require:
      - musthaves.git - Install {{musthaves.gitpkg}} package

{% for dir in etcVim, "/home/" ~ pillar.user ~ "/.vim" %}
{%   for scripttype in "ftdetect", "ftplugin", "syntax" %}

{{sls}} - Symlink sls {{scripttype}} in {{dir}} for Vim:
  file.symlink:
    - name: {{dir}}/{{scripttype}}/sls.vim
    - target: {{srcDir}}/{{scripttype}}/sls.vim
    - makedirs: True
    - user: {{pillar.user}}
    - group: {{pillar.user}}  # TODO:  Sloppy, assumes group = user

{%   endfor %}  # script types
{% endfor %}    # directories

{{sls}} - Install pylint:
  pip.installed:
    - pkgs:
      - saltpylint
    - bin_env: /usr/bin/pip
