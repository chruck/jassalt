include:
  - gpg
#  - .otherSubstate

#{{sls}} - Show /etc/resolv.conf:
#  cmd.run:
#    - name: "ls -al /etc/resolv.conf; cat /etc/resolv.conf"

{{sls}} - Install https transport for apt:
  pkg.installed:
    - allow_updates: True
    - pkgs:
      - apt-transport-https
#      - gnupg  # for pkgrepo's key_url
#      - apt-utils
#      - kmod
#      - openresolv
#      - ifupdown2

# Debian 10 dropped support for monit (systemd replaces, but
# openmediavault still depends on monit)
{% if "Debian-10" == grains.osfinger %}
{{sls}} - Add old stretch package repo:
  pkgrepo.managed:
    - humanname: stretch repository
    - name: deb http://deb.debian.org/debian stretch main
    - file: /etc/apt/sources.list.d/stretch.list
{% endif %}  # Debian-10

{{sls}} - Add openmediavault package repo:
  pkgrepo.managed:
    - humanname: openmediavault repository
#    - name: deb https://packages.openmediavault.org/public shaitan main partner
    - name: deb https://packages.openmediavault.org/public shaitan main
#    - comps:  main,partner
#    - dist: stretch
    - file: /etc/apt/sources.list.d/openmediavault.list
    - key_url: https://packages.openmediavault.org/public/archive.key
    - require:
      - {{sls}} - Install https transport for apt

{{sls}} - Install openmediavault keyring package:
  pkg.installed:
    - name: openmediavault-keyring
    - allow_updates: True
    - skip_verify:  True
    - require:
      - {{sls}} - Add openmediavault package repo

{% for i in [
        "openmediavault-beep-down",
        "openmediavault-beep-up",
        "openmediavault-cleanup-monit",
        "openmediavault-cleanup-php",
        "openmediavault-engined",
        "openmediavault-issue",
        ] %}
#{{sls}} - Mask service {{i}} before installing:
#  service.unmasked:
#    - name: {{i}}
#    - runtime: True
{% endfor %}

{{sls}} - Install openmediavault package:
  pkg.installed:
    - name: openmediavault
    - allow_updates: True
    - refresh: True
#  cmd.run:
#    - name: DEBIAN_FRONTEND=noninteractive APT_LISTCHANGES_FRONTEND=none apt-get --yes --force-yes --auto-remove --show-upgraded  --no-install-recommends  --option Dpkg::Options::="--force-confdef"  --option DPkg::Options::="--force-confold" install postfix openmediavault
    - require:
      - {{sls}} - Install openmediavault keyring package
      - {{sls}} - Add openmediavault package repo

#{{sls}} - Install postfix package:
#  pkg.installed:
#    - name: postfix
#    - require:
#      - {{sls}} - Install openmediavault keyring package

# TODO:  Not sure what replaces this:
#{{sls}} - Initialize the system and database:
#  cmd.run:
#    - name:  omv-initsystem
#    - require:
#      - {{sls}} - Install openmediavault package
