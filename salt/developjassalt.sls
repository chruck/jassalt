# salt/developjassalt.sls

# State to make a minion be a workstation to develop this codebase,
# "jassalt":

{% from "pkgnames_map.jinja" import pkg_name with context %}
{% from "salt/map.jinja" import jassaltDir with context %}

include:
  - salt.jassalt
  - precommit

{{sls}} - Install pip:
  pkg.installed:
    - name: {{pkg_name.pip}}

{#
{{sls}} - Install pre-commit pkg:
  pip.installed:
    - name: pre-commit
    - bin_env: /usr/bin/pip
    - require:
      - {{sls}} - Install pip
#}

{{sls}} - Install pre-commit Git hooks:
  cmd.run:
    - name: cd {{jassaltDir}}; pre-commit install; #pre-commit autoupdate
    - require:
      - precommit - Install pre-commit pkg
    - creates: {{jassaltDir}}/.git/hooks/pre-commit
