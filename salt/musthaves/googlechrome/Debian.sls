# salt/musthaves/googlechrome/Debian.sls

{% if "Debian" != grains.os_family %}

{{sls}} - Google Chrome browser:
  test.nop:
    - name: skipping, not Debian ({{grains.os_family}})

{% else %}  # Debian

include:
  - gpg

{{sls}} - Must-Haves for Desktop, Google Chrome repo, Debian:
  pkgrepo.managed:
    - humanname: GoogleChrome
    #- name: deb http://dl.google.com/linux/chrome/deb/ stable main
    - name: deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main
    - key_url: https://dl-ssl.google.com/linux/linux_signing_key.pub
    - file: /etc/apt/sources.list.d/google-chrome.list
    - dist: stable
    - gpgcheck: 1
    - require:
      - gpg - Install GPG

{{sls}} - Must-Haves for Desktop, Google Chrome, Debian:
  pkg.installed:
    - name: google-chrome-stable
    - refresh: True
#    - install_recommends: False
#    - uses: X
    - require:
      - {{sls}} - Must-Haves for Desktop, Google Chrome repo, Debian

{% endif %}  # Debian
