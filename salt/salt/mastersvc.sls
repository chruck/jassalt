{% from "salt/map.jinja" import atinstallID, mastersvcID with context %}

include:
  - .at

{{mastersvcID}}:
  at.watch:
    - job: 'salt-call --local service.restart salt-master'
    - timespec: 'now +1 min'
    - tag: master_restart
    - unique_tag: true
    - onlyif: |
        test "unknown" != "{{grains.init}}" -a "{{salt.pkg.version("salt-master")}}"
    - require:
      - {{atinstallID}}
