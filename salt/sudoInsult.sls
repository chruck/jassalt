include:
  - gentoo.sudo

{% if "Gentoo" != grains.os %}

{{sls}} - Install sudo package:
  pkg.installed:
    - name: sudo
    - refresh: True
    - aggregate: True
    - require_in:
      - {{sls}} - Set sudo to insult you when you put in a bad password

{% endif %}  # not Gentoo

{{sls}} - Set sudo to insult you when you put in a bad password:
  file.managed:
    - name: /etc/sudoers.d/insults
    - contents: Defaults insults
