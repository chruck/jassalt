#!/bin/bash

# cicd/setupFuntooContainer.sh

##
# @file setupFuntooContainer.sh
# @author Jas Eckard <jas+jassalt@eckard.com>
#
# @section LICENSE
#
# (GPL license)
#
# @section DESCRIPTION
#
# This script sets up a Funtoo container for GitLab CI to test.
# Specifically, for the 'installArchAtopFuntoo' states.

# Return codes:  1 = bad parameter(s)
#                2 = required parameter missing
#                3 = distribution other than Funtoo, or no /etc/os-release
#                4 = not running in a container

#readonly localDir="$(cd ${BASH_SOURCE[0]%/*} && pwd -P && cd - >/dev/null)"

# To get some debug output call this file like:
# DEBUG=true ./setupFuntooContainer.sh ...

echoStderr() { echo "$@" 1>&2; }

if [[ '' == "${DEBUG}" ]]; then
        # Make `debug' a no-op
        debug() { :; }
else
        debug() {
                echoStderr -n "DEBUG ${BASH_LINENO[*]}:  "
                echoStderr "$@"
        }
fi

# Usages:
#echo “regular stdout output”
#echoStderr “regular stderr output”
#debug “stderr output only seen when DEBUG set”

declare rc=0
#declare optionA=0
#declare optionB=bogus
#declare REQUIRED
#declare OPTIONAL=bogus
#readonly pi="3.1415"

Usage()
{
        cat 1>&2 <<EOFUsage

#Usage:  $0 [OPTION]... REQUIRED [OPTIONAL]

# -a, --optionA         Description
# -b, --optionB VAL     Desc that takes VAL
  -h, -?, --help        Display this help and exit

EOFUsage
        exit ${rc}
}  # Usage()

parseOptions()
{
        debug "Original args:  $*"

        local shortopts="h?" ; local longopts="help,"
#       shortopts+="a"; longopts+="optionA,"
#       shortopts+="b:"; longopts+="optionB:,"

        debug "shortopts=${shortopts} longopts=${longopts}"

        ARGS=$(getopt -o "${shortopts}" -l "${longopts}" -n "getopt.sh" -- "$@")

        getoptRc=$?
        if [[ 0 -ne "${getoptRc}" ]]; then
                echoStderr "ERROR:  getopt called incorrectly, rc=${getoptRc}"
                rc=1
                exit ${rc}
                # Alternatively, ($rc gets passed):
                #Usage
        fi

        eval set -- "${ARGS}"

        while true; do
                param=$1
                case "${param}" in
#                       -a|--optionA)
#                               debug "-a used"
#                               optionA=1
#                               shift
#                               ;;
#                       -b|--optionB)
#                               shift
#                               if [[ -n "$1" ]]; then
#                                       debug "-b used: $1";
#                                       optionB=$1
#                                       shift;
#                               fi
#                               ;;
                        -h|-\?|--help)
                                debug "Asked for help"
                                Usage
                                ;;
                        --)
                                shift;
                                break;
                                ;;
                        *)
                                echoStderr "ERROR:  Unimplemented option: ${param}"
                                break;
                                ;;
                esac
        done

        debug "New args:  $*"

#       # For 1 required and one optional argument
#       if [[ 1 != "${#@}" && 2 != "${#@}" ]]; then
#               rc=2
#               echoStderr "ERROR:  REQUIRED required."
#       fi

#       REQUIRED=$1
#       OPTIONAL=$2

#       if [[ 'bogus' == "${optionB}" ]]; then
#               rc=2
#               echoStderr "ERROR:  --optionB field required."
#       fi

        if [[ 0 != "${rc}" ]]; then
                Usage
        fi
}  # parseOptions()

# TODO:  Verify the parameters passed are valid
verifyParams()
{
        :
#       if [[ "${optionB}" =~ /reg[ex]/ ]]; then
#               echoStderr "ERROR:  --optionB should be of the form: reg[ex]"
#               rc=2
#       fi
}  # verifyParams()

#        while read line; do
#                echoStderr "WARNING:  I do not like ${line}"
#        done < <(grep "^${softwarebom}" ${steutabFName})

verifyFuntoo()
{
        # shellcheck source=/dev/null
        source /etc/os-release
        if [[ "funtoo" != "${ID}" ]]; then
                echoStderr "Not running on Funtoo, exiting"
                exit 3
        fi
}

verifyContainer()
{
        if ! grep container /proc/self/mountinfo; then
                echoStderr "Not running in a container, exiting"
                exit 4
        fi
}

setupJas()
{
        emerge sudo
        useradd jas
        echo "jas ALL=(ALL:ALL) ALL" >/etc/sudoers.d/jas
        # shellcheck disable=SC2016
        sed -i '/jas/s?!?$6$ZOjPkUIK6vwzVaZg$jYnNq3DXtmih7jcZD9Ccr7YCJIAbgu/l3BekdZJ6QhnOsZ6est14Ik7vFQbS3XFrMJEPU0xk3cvyJ0MKmpEyI0?' /etc/shadow
        # start sshd
        # ssh jas@localhost
}  # setupJas()

installSalt()
{
        export PATH=${PATH}:/root/.local/bin
        # pip install salt --break-system-packages
        pip install salt --user
        salt-call --local test.ping
        salt-call --local test.version
        salt-call --version
}  # installSalt()

doSetup()
{
        ego sync
        PYTHON_TARGETS=python3_7 emerge dev-python/pip

        # Not needed for CI/CD, right?
        # setupJas

        installSalt
}  # doSetup()

setupFuntooContainer()
{
        verifyFuntoo
        verifyContainer
        doSetup
}

parseOptions "$@"
verifyParams
setupFuntooContainer
