{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import mntPt, with context %}
{%   from tpldir ~ "/iscontainer.jinja" import is_container with context %}

include:
  - .installingABootloader

{%   set cmdrun = "cmd.run" %}

{%   if is_container %}

{%     set cmdrun = "test.succeed_with_changes" %}

{{sls}} - Running in container:
  test.succeed_with_changes:
    - name: Not installing grub inside a container

{%   endif %}  # container

{{sls}} - Install grub to MBR of {{pillar.funtoodisk}}:
  {{cmdrun}}:
    - name: /bin/chroot {{mntPt}} grub-install --target=i386-pc --no-floppy {{pillar.funtoodisk}}
    - require:
      - installFuntoo.installingABootloader - Install grub program

{{sls}} - Generate /boot/grub/grub.cfg:
  {{cmdrun}}:
    - name: /bin/chroot {{mntPt}} boot-update
    - require:
      - installFuntoo.installingABootloader - Update /etc/boot.conf

{% endif %}  # hostnameIsCorrect
