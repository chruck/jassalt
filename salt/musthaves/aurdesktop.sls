# salt/musthaves/aurdesktop.sls

{% if "Arch" != grains.os_family %}

{{sls}} - Install Desktop Must-Haves via AUR - not Arch ({{grains.os_family}}):
  test.nop

{% else %}  # Arch

{%   for pkg, path in [
        ["electricsheep", "/usr/bin/electricsheep"],
        ["tcllib", "/usr/bin/tcllib"],
] %}
{{sls}} - Install Desktop Must-Haves via AUR - {{pkg}}:
  cmd.run:
    - name: pamac build --no-confirm {{pkg}}
    - onlyif:
      # Use `pamac search -a` to prime pamac database with AUR.
      # `pamac` return 0 even if not found, but since not found gives no
      # output, test for empty string:
      - pamac search -a {{pkg}} |grep "^{{pkg}} "
      - "! test -x {{path}}"
{%   endfor %}  # pkgs/paths

{% endif %}  # Arch
