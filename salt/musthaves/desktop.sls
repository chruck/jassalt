# salt/musthaves/desktop.sls

{% from "iscontainer.jinja" import is_container with context %}
{% from tpldir ~ "/map.jinja" import musthaves, with context %}
{% from tpldir ~ "/vars.jinja" import musthaves4Desktop, with context %}

include:
  - gentoo
  - musthaves
  - .aurdesktop
  - .barrier
  - .browserextensions
  - .firefox
  - .git
  - .googlechrome
  - .pip_progs

# TODO:  For Bodhi Linux:  don't install 'chromium-browser', instead
#        'bodhi-chromium' (check if dir /etc/bodhi exists)
# TODO:  For Bodhi Linux:  upgrading package 'enlightenment' is
#        problematic, this needs to be fixed
{{musthaves4Desktop}}:
  pkg.installed:
    - refresh: True
#    - install_recommends: False
    - pkgs: {{musthaves.desktoplist + musthaves.desktopbrowserlist}}

{{sls}} - Enable and Start services:
  service.running:
    - names: {{musthaves.desktopservices}}
    - enable: True
    - onlyif: test "True" != {{is_container}}
    - require:
      - {{musthaves4Desktop}}
