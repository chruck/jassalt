{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{% from tpldir ~ "/vars.jinja" import
        hostnameFile,
        mountAsFuntoo,
        mountingFilesystems,
        with context %}

include:
  - {{mountingFilesystems}}

{{sls}} - Change hostname to '{{opts.id}}':
  file.replace:
    - name: {{hostnameFile}}
    - pattern: "hostname=.*"
    - repl: "hostname={{opts.id}}"
    - require:
      - {{mountAsFuntoo}}

{% endif %}  # hostnameIsCorrect
