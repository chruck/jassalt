{% if "Windows" == grains.os_family %}

# Separated out to satisfy another state
{%   from tpldir ~ "/map.jinja" import musthaves with context %}

{{sls}} - Install {{musthaves.gitpkg}} package:
  cyg.installed:
    - name: {{musthaves.gitpkg}}

{%   if musthaves.gitpkgs is defined %}
{%     for pkg in musthaves.gitpkgs %}
{{sls}} - Install {{pkg}} package:
  cyg.installed:
    - name: {{pkg}}
{%     endfor %}  # git packages loop
{%   endif %}     # git packages

{% endif %}       # Windows
