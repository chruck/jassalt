# salt/salt/virt.sls

# TODO:  This should be replaced by gitfs formula 'libvirt' and
#        anything else that it doesn't do right should either be
#        pillar'd or added here.

{% from "iscontainer.jinja" import is_container with context %}
{% from "salt/map.jinja" import libVirt with context %}
{% from "pkgnames_map.jinja" import pkg_name with context %}

include:
  - gentoo
#  - .br0
  - .salt-pip

{{sls}} - Install libvirt:
  pkg.installed:
    - pkgs: {{pkg_name.libvirt}}

{{sls}} - Config libvirt:
  file.append:
    - name: {{libVirt.conffile}}
    - text: libvirtd_opts="-l"
    - require:
      - {{sls}} - Install libvirt

{#
{% if is_container %}
#}

{{sls}} - Install pkg-config:
  pkg.installed:
    - name: {{pkg_name.pkg_config}}
    # Only needed if using salt-pip version of libvirt-python:
    - onlyif:
      - salt-pip
      - "! ( salt-pip list |grep libvirt )"

{{sls}} - Install libvirt-python:
  pkg.installed:
    - name: {{pkg_name.libvirt_python}}

# TODO:  add this to libvirt formula
{{sls}} - For Onedir Salt, add libvirt-python via pip:
  cmd.run:
    - name: salt-pip install libvirt-python
    - onlyif:
      - salt-pip
      - "! ( salt-pip list |grep libvirt )"
    - require:
      - {{sls}} - Install libvirt
      - {{sls}} - Install pkg-config
      - {{sls}} - Install libvirt-python

{#
{% endif %}  # is_container
#}

{{sls}} - Install libguestfs:
  pkg.installed:
    - pkgs: {{pkg_name.libguestfs}}

{{sls}} - Daemonize libvirt:
  service.running:
    - name: libvirtd
    - onlyif: "! {{is_container | lower}}"
    - unmask: True
    - require:
      - {{sls}} - Install libvirt
{% if grains.os_family in [ "Debian", "RedHat", ] %}
#      - network: br0
{% endif %}  # Deb or RH for salt.br0
      # Dunno where this came from:
      #- libvirt: libvirt
      - {{sls}} - For Onedir Salt, add libvirt-python via pip
    - watch:
      - {{sls}} - Config libvirt

{{sls}} - Manage libvirt keys:
  virt.keys:
    - name:  libvirt_keys
    - require:
      - {{sls}} - Install libvirt
      - {{sls}} - For Onedir Salt, add libvirt-python via pip
      - {{sls}} - Install libvirt-python
      - {{sls}} - Daemonize libvirt
    - watch:
      - {{sls}} - Daemonize libvirt
