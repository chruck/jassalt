{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        filesystemtype,
        mntFstab,
        mountAsFuntoo,
        mountingFilesystems,
        with context %}

include:
  - {{mountingFilesystems}}

cat /mnt/funtoo/etc/fstab:
  cmd.run

{{sls}} - Comment all {{pillar.funtoodisk}}*'s from {{mntFstab}}:
  file.replace:
    - name: {{mntFstab}}
    - pattern: '^([^#].*{{pillar.funtoodisk}})'
    - repl: '# \1'
    - require:
      - {{mountAsFuntoo}}
      - cat /mnt/funtoo/etc/fstab

{{sls}} - Comment swap from {{mntFstab}}:
  file.replace:
    - name: {{mntFstab}}
    - pattern: '^([^#].*swap)'
    - repl: '# \1'
    - require:
      - {{mountAsFuntoo}}

#{{sls}} - Remove /boot from {{mntFstab}}:
#  #file.comment:
#  mount.unmounted:
#    - name: /boot
#    - config: {{mntFstab}}
#    - persist: True
#    - require:
#      - {{mountAsFuntoo}}

{{sls}} - Add / to {{mntFstab}}:
  mount.mounted:
    - name: /
    - mount: False
    - config: {{mntFstab}}
    - device: {{pillar.funtoodiskslash}}
    - fstype: {{filesystemtype}}
    - dump: 1
    - opts: rw,relatime,ssd,space_cache,subvolid=5,subvol=/
    - pass_num: 1
    - require:
      - {{mountAsFuntoo}}

{#
{{sls}} - Add /proc to {{mntFstab}}:
  mount.mounted:
    - name: /proc
    - mount: False
    - config: {{mntFstab}}
    - device: proc
    - fstype: proc
    - require:
      - {{mountAsFuntoo}}

{{sls}} - Add /sys to {{mntFstab}}:
  mount.mounted:
    - name: /sys
    - mount: False
    - config: {{mntFstab}}
    - device: /sys
    - fstype: sysfs
    - require:
      - {{mountAsFuntoo}}

{{sls}} - Add /dev to {{mntFstab}}:
  mount.mounted:
    - name: /dev
    - mount: False
    - config: {{mntFstab}}
    - device: /dev
    - fstype: devtmpfs
    - require:
      - {{mountAsFuntoo}}
#}

{% endif %}  # hostnameIsCorrect
