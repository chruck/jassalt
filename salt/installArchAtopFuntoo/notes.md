# Process to install Arch Linux on top of a running Funtoo system

---

## Testing system

### Funtoo Linux container

Tests conducted on a Docker image of Funtoo Linux:

```shell
$ docker run --rm -d funtoo/stage3-generic_64
# or
$ podman run --network=host --rm -d docker.io/funtoo/stage3-generic_64
```

This starts a container of Funtoo as a daemon that has OpenRC
running. Enter that container and update it:

```shell
$ ${docker} ps |grep funtoo
$ ${docker} exec -it ${hash_or_name} /bin/bash
# ego sync
# emerge sudo
# useradd jas
# echo "jas ALL=(ALL:ALL) ALL" >/etc/sudoers.d/jas
# passwd jas
{
  # if using podman with host network, config port 2222:
  # sed -i 's/#Port 22/Port 2222/' /etc/ssh/sshd_config
  # /etc/init.d/sshd restart
)
# su - jas   # to confirm
$ ip a       # to get IP, then from somewhere else (eg, Debian
             # container, below):
(might need to:
 $ ssh-keygen -f "/home/jas/.ssh/known_hosts" -R "172.17.0.2"
)
$ ssh jas@${ip} # -p2222
```

NOTE: Updated above commands to `Dockerfile.funtoo` and
`Containerfile.funtoo`, to be built via:

```shell
docker build . -f Dockerfile.funtoo -t ${TAG}
podman build . -f Containerfile.funtoo -t ${TAG}
```

respectively, then start the container the same way:

```shell
${docker} run --rm -d funtoo/stage3-generic_64
${docker} exec -it ${hash_or_name} /bin/bash
```

### Salt Master Linux container

Tests conducted on a Docker image of Debian:

```shell
$ docker run --rm -it debian /bin/bash
# apt update
# apt install -y openssh-client pip  # or python3-pip
# pip install heist-salt --break-system-packages
# cat <<EOF >roster.cfg
> funtoo:
>   host: 172.17.0.2
>   username: jas
>   password: global
> EOF
# salt-master
(# salt-master -linfo)
(# salt-master -d)
```

This will have the salt master daemon running in the foreground.
Start another terminal and reattach to the Debian container, by
finding which one it is:

```shell
$ docker ps |grep debian
$ docker exec -it ${hash_or_name} /bin/bash
# salt-key  # verify saltmaster is running, see if any minions
# heist salt.minion -R roster.cfg  # deployed in foreground
(# heist salt.minion -Rroster.cfg -linfo)
(# salt-run heist.deploy salt.minion  # ?)
```

Now connect a third time:

```shell
$ docker ps |grep debian
$ docker exec -it ${hash_or_name} /bin/bash
# salt-key  # verify saltmaster is running, see if any minions
```

Teardown:

```shell
docker kill ${funtoo_hash_or_name}
```

### Interactive Funtoo container

```shell
podman run --entrypoint /bin/bash --volume /home/jas/src/jassalt:/srv \
        --network=host --rm -it docker.io/funtoo/stage3-generic_64
```

If wanting to mount a file as a btrfs filesystem add some combination
of (not sure which):

```shell
--cap-add SYS_ADMIN
--privileged
--security-opt apparmor:unconfined
--security-opt apparmor=unconfined
--security-opt unmask:all  # podman only?
--security-opt unmask=all  # podman only?
--volume /home/jas/src/jassalt:/srv:z
--volume /home/jas/src/jassalt:/srv:Z
-–security-opt seccomp:unconfined
-–security-opt seccomp=unconfined
```

## Now Install Arch Linux from existing Linux

<https://wiki.archlinux.org/title/Install_from_existing_Linux>

### 1) archstrap

<https://github.com/wick3dr0se/archstrap>
