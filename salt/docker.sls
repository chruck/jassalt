# salt/docker.sls

# State to install docker/podman

# TODOs:
# - Docker/Podman Formula?
# - Docker Compose?
# - Install Docker repo?
# - Install Docker or Podman
# - Add 'jas' to 'docker' group

{{sls}} - Install podman-compose:
  pkg.installed:
    - name: podman-compose
