# salt/musthaves/screen.sls

# Screen sessions that are started under an X11 display manager have
# been getting killed when the DM is killed.  This may be Debian-only,
# but it seems that SystemD is killing them, so according to
# https://askubuntu.com/questions/984332/something-kills-my-screens
# these settings need to be changed:

# /etc/systemd/logind.conf:
# Update to:
# KillUserProcesses=no
# KillExcludeUsers=root YOUR_USERNAME

# Then run the command:
# $ sudo loginctl enable-linger YOUR_USERNAME
