# Download VMware Workstation Bundle (and verify checksums) at:
# https://customerconnect.vmware.com/en/downloads/info/slug/desktop_end_user_computing/vmware_workstation_pro/16_0
# 17: https://www.vmware.com/go/getworkstation-linux
# License key from:  https://velp.eng.vmware.com/my/licenses

# Assumes VMware-Workstation-Full-${version}.x86_64.bundle is in
# /home/jas/Downloads:
{% set installer = salt.file.find("/home/jas/Downloads",
                             name="VMware-Workstation-Full-*.x86_64.bundle")
%}
{% set licensefile = salt.file.find("/home/jas/Downloads",
                             name="VMware-Workstation-Full-*.license")
%}

{% if not installer or not licensefile %}

{{sls}} - Need installer and/or license:
  test.fail_without_changes:
    - name: >
        Missing VMware-Workstation-Full-*.x86_64.bundle and
        VMware-Workstation-Full-*.license (manually created text file)
        in /home/jas/Downloads (found '{{installer}}' and
        '{{licensefile}}')

{% else %}  # found both installer and license file:

{%   set license = salt.file.read(licensefile[0]) %}

{{sls}} - Install VMware Workstation:
  cmd.run:
    - name: /bin/bash {{installer[0]}} --eulas-agreed --set-setting vmware-workstation serialNumber {{license}}
    - creates: /usr/bin/vmware

# openssl req -new -x509 -newkey rsa:2048 -keyout MOK.priv -outform DER -out MOK.der -nodes -days 36500 -subj "/CN=VMware/"
# sudo /usr/src/linux-headers-`uname -r`/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n vmmon)
# sudo /usr/src/linux-headers-`uname -r`/scripts/sign-file sha256 ./MOK.priv ./MOK.der $(modinfo -n vmnet)
# sudo mokutil --import MOK.der  # with password
# reboot # then sign SecureBoot with that password

{% endif %}  # installer and license files
