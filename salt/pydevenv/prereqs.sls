{% from tpldir ~ "/map.jinja" import
        installprereqs,
        pydevenvpkgs,
        with context %}

{{installprereqs}}:
  pkg.installed:
    - pkgs: {{pydevenvpkgs}}
