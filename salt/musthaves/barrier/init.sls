# salt/musthaves/barrier/init.sls

include:
  - gentoo

{% set barrierServer = "tiger" %}
{% set barrierServerIP = "172.16.16.16" %}
{% set barrierClientR = "crystal" %}
{# Wireless IP:
   set barrierClientRIP = "172.16.16.236" #}
{% set barrierClientRIP = "172.16.16.209" %}
{% set barrierClientRFQDN = "crystal.eckard.local" %}
{% set barrierClientRR = "sugar" %}
{# Wireless IP:
   set barrierClientRRIP = "172.16.16.182" #}
{% set barrierClientRRIP = "172.16.16.228" %}
{% set barrierClientRRFQDN = "sugar.eckard.local" %}
{% set barrierClientRRR = "modern" %}
{# Wireless IP:
   set barrierClientRRRIP = "172.16.16.169" #}
{% set barrierClientRRRIP = "172.16.16.112" %}
{% set barrierClientRRRFQDN = "modern.tc" %}

{% from "iscontainer.jinja" import is_container with context %}
{% if grains.nodename in
        [barrierServer, barrierClientRR, barrierClientR, barrierClientRRR]
        and not is_container %}

{%   if grains["host"] == barrierServer %}
{%      set daemon = "Server" %}
{%      set command = "sudo -u " ~ pillar['user'] ~ " sh -c 'pkill barriers; DISPLAY=:0.0 barriers --disable-crypto'" %}
{%   elif grains["host"] in
        [ barrierClientR, barrierClientRR, barrierClientRRR, ] %}
{%      set daemon = "Client" %}
{#      set command = "sudo -u " ~ pillar['user'] ~ " sh -c 'pkill barrierc; DISPLAY=:0.0 barrierc " ~ barrierServer ~ "'" #}
{%      set command = "sudo -u " ~ pillar['user'] ~ " sh -c "
                      ~ "'pkill barrierc; "
                          ~ "DISPLAY=:0.0 barrierc --disable-crypto "
                                ~ barrierServer ~ "'" %}
{%   endif %}  # Barrier cmdline based on hostname

{{sls}} - Install Barrier:
  pkg.installed:
    - name: barrier
    - refresh: True
    - install_recommends: False

{{sls}} - Add Barrier config file:
  file.managed:
    - name: /etc/barrier.conf
    - source: salt://{{ sls|replace(".", "/") }}/barrier.conf.jinja
    - template: jinja
    - defaults:
        barrierServer: {{barrierServer}}
        barrierServerIP: {{barrierServerIP}}
        barrierClientR: {{barrierClientR}}
        barrierClientRIP: {{barrierClientRIP}}
        barrierClientRFQDN: {{barrierClientRFQDN}}
        barrierClientRR: {{barrierClientRR}}
        barrierClientRRIP: {{barrierClientRRIP}}
        barrierClientRRFQDN: {{barrierClientRRFQDN}}
        barrierClientRRR: {{barrierClientRRR}}
        barrierClientRRRIP: {{barrierClientRRRIP}}
        barrierClientRRRFQDN: {{barrierClientRRRFQDN}}

{{sls}} - Start Barrier {{daemon}}:
  cmd.run:
    - name: {{command}}
    - onchanges:
      - {{sls}} - Add Barrier config file

{% endif %}  # nodename is tiger, sugar, crystal, or modern
