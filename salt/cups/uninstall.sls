# CUPS printer daemon uninstallation state

{{sls}} - Uninstall CUPS software:
  pkg.purged:
    - name: cups
