include:
  - .prereqs

{% from tpldir ~ "/map.jinja" import
        installprereqs,
        with context %}

{#
{% set pythonver = "3.9.7" %}
{% set pythonver = "system" %}
#}
{% set pythonver = "3.10.6" %}
{% set projectname = "locke" %}

#{{sls}} - Clone pyenv git environment into user's home directory:
#  git.latest:
#    - name: https://github.com/pyenv/pyenv.git
#    - target: /home/{{pillar.user}}/.pyenv
#    - force_reset: True
#    - require:
#      - {{installprereqs}}

## Hopefully this will take care of the above:
#{{sls}} - Install pyenv:
#  pyenv.install_pyenv:
#    - name: {{pythonver}}
#    - user: {{pillar.user}}

{{sls}} - Install Python with pyenv:
  pyenv.installed:
    - name: {{pythonver}}
    - user: {{pillar.user}}
##    - default: True
    - require:
      - {{installprereqs}}

{{sls}} - Clone pyenv-virtualenv git environment into pyenv plugins directory:
  git.latest:
    - name: https://github.com/pyenv/pyenv-virtualenv.git
    - target: /home/{{pillar.user}}/.pyenv/plugins/pyenv-virtualenv
    - force_reset: True
    - user: {{pillar.user}}
    - require:
      - {{installprereqs}}
#      - {{sls}} - Clone pyenv git environment into user's home directory
#      - {{sls}} - Install pyenv
      - {{sls}} - Install Python with pyenv

{{sls}} - Setup pyenv:
  test.show_notification:
    - name: Add to profile
    - text: |
        Be sure to add the following to your .bash_profile:
        export PYENV_ROOT="$HOME/.pyenv"
        PATH=$PATH:$HOME/.local/bin:$HOME/bin:$PYENV_ROOT/bin
        if command -v pyenv 1>/dev/null 2>&1; then
          eval "$(pyenv init -)"
          eval "$(pyenv virtualenv-init -)"
        fi

{{sls}} - Virtual Environment Setup - virtualenv:
  cmd.run:
    - name: cd /home/{{pillar.user}}/src/locke; pyenv virtualenv {{pythonver}} {{projectname}}
    - runas: {{pillar.user}}
    - onlyif: test -d /home/{{pillar.user}}/src/locke
    - require:
      - {{sls}} - Install Python with pyenv

{{sls}} - Virtual Environment Setup - local:
  cmd.run:
    - name: cd /home/{{pillar.user}}/src/locke; pyenv local {{projectname}}
    - runas: {{pillar.user}}
    - onlyif: test -d /home/{{pillar.user}}/src/locke
    - require:
      - {{sls}} - Virtual Environment Setup - virtualenv
