{% from tpldir ~ "/vars.jinja" import
        bootMntPt,
        filesystemtype,
        formatDisk,
        fixFstab,
        mountAsFuntoo,
        mountBootPart,
        mntFstab,
        mntPt,
        prepareDisk,
        rmMntPtFromFstab,
        with context %}
{% from tpldir ~ "/iscontainer.jinja" import is_container with context %}

{% if is_container %}

{{mountAsFuntoo}}:
  test.show_notification:
    - name: Not mounting a {{filesystemtype}} inside a container
    - text: Containers don't have '{{filesystemtype}}'

{% else %}

{%   from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{%   if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{%   else %}  # hostnameIsCorrect

include:
  - {{prepareDisk}}
  - {{fixFstab}}

{{mountAsFuntoo}}:
  mount.mounted:
    - name: {{mntPt}}
    - device: {{pillar.funtoodiskslash}}
    - fstype: {{filesystemtype}}
    #- pass_num: 1
    - mkmnt: True
    - config: {{mntFstab}}
    - require:
      - {{formatDisk}}
    - require_in:
      - {{rmMntPtFromFstab}}

{{mountBootPart}}:
  mount.mounted:
    - name: {{bootMntPt}}
    - device: {{pillar.funtoodiskboot}}
    - fstype: vfat
    - mkmnt: True
    - config: {{mntFstab}}
    - require:
      - {{mountAsFuntoo}}
    - require_in:
      - {{rmMntPtFromFstab}}

{%   endif %}  # hostnameIsCorrect
{% endif %}    # container?
