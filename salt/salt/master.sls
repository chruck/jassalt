{% from "salt/map.jinja" import
        gitlabURL, jassaltDir, srcDir, mastersvcID with context %}
{% from "musthaves/map.jinja" import musthaves with context %}
{% from "pkgnames_map.jinja" import pkg_name with context %}

{% set srvDir = "/srv" %}
{% set saltDir = srvDir ~ "/salt" %}
{% set pillarDir = srvDir ~ "/pillar" %}
{% set bashrcDir = srcDir ~ "/dot.bashrc.jas" %}
{% set masterDir = "/etc/salt/master.d" %}

include:
  - bashrc
  - gentoo
  - musthaves.git
  - .bashcomplete
  - .gitfs
  - .jassalt
  - .mastersvc
  - .minion
  - .subdirs

{{sls}} - Install salt-master pkg:
  pkg.installed:
    - name: {{pkg_name.salt_master}}
    - refresh: True
    - require_in:
      - {{mastersvcID}}

{{sls}} - Set hash to SHA512:
  file.managed:
    - name: {{masterDir}}/hash.conf
    - contents: "hash_type: sha512"
    - require:
      - {{sls}} - Install salt-master pkg
      - salt.subdirs - Create salt subdirectories
    - watch_in:
      - {{mastersvcID}}

{{sls}} - Set file_ignore:
  file.managed:
    - name: {{masterDir}}/file_ignore.conf
    - source: salt://salt/file_ignore.conf
    - require:
      - {{sls}} - Install salt-master pkg
      - salt.subdirs - Create salt subdirectories
    - watch_in:
      - {{mastersvcID}}

{{sls}} - Create {{srvDir}}:
  file.directory:
    - name: {{srvDir}}

{{sls}} - Symlink for {{saltDir}}:
  file.symlink:
    - name: {{saltDir}}
    - target: {{jassaltDir}}/salt
    - onlyif: test ! -e {{saltDir}}/top.sls
    - require:
      - {{sls}} - Create {{srvDir}}

{{sls}} - Symlink for {{pillarDir}}:
  file.symlink:
    - name: {{pillarDir}}
    - target: {{jassaltDir}}/pillar
    - onlyif: test ! -e {{pillarDir}}/top.sls
    - require:
      - {{sls}} - Create {{srvDir}}

{{sls}} - Pull down the latest .bashrc.jas:
  git.latest:
    - name: {{gitlabURL}}/dot.bashrc.jas.git
    - target: {{bashrcDir}}
    - force_reset: True
    - require:
      - "musthaves.git - Install {{musthaves.gitpkg}} package"
    - require_in:
      - "bashrc - Upload root's .bashrc.jas"
      - "bashrc - Upload Jas' .bashrc.jas"

{% set rc_srv = saltDir ~ "/bashrc/.bashrc.jas" %}

{{sls}} - Delete symlink for {{rc_srv}}:
  file.absent:
    - name: {{rc_srv}}
    - onlyif: test -L {{rc_srv}} -o ! -f {{rc_srv}}
    - require:
      - "{{sls}} - Symlink for {{saltDir}}"
      - "{{sls}} - Pull down the latest .bashrc.jas"

{{sls}} - Copy {{rc_srv}} from {{bashrcDir}}:
  file.managed:
    - name: {{rc_srv}}
    - source: {{bashrcDir}}/.bashrc.jas
    - require:
      - "{{sls}} - Symlink for {{saltDir}}"
      - "{{sls}} - Pull down the latest .bashrc.jas"
      - "salt.jassalt - Pull down the latest jassalt salt states"
      - "{{sls}} - Delete symlink for {{rc_srv}}"
    - require_in:
      - "bashrc - Upload root's .bashrc.jas"
      - "bashrc - Upload Jas' .bashrc.jas"
