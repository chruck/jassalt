# This file is part of OpenMediaVault.
#
# @license   http://www.gnu.org/licenses/gpl.html GPL Version 3
# @author    Volker Theile <volker.theile@openmediavault.org>
# @copyright Copyright (c) 2009-2022 Volker Theile
#
# OpenMediaVault is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# OpenMediaVault is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with OpenMediaVault. If not, see <http://www.gnu.org/licenses/>.

# OMV top file merged with jassalt top file by Jas:

base:
  # Only needs saltminion (alarm doesn't need 'musthaves', eg)
  '*':
#    - salt
    - grub
    - adduser
    # users  # TODO:  Replace 'adduser' above, cfg master w/ 'users' formula

  # Desktops
  grace,tiger,jas,sugar,crystal,manjaroxfce,modern:
    - match: list
    - bashrc
    # clemson
    - developjassalt
    - musthaves
    - musthaves.desktop
    - musthaves.vlc
    - salt.edit
    - sudoInsult
    - obsidian

  # Laptops:
  sugar,crystal,cider,manjaroxfce,modern:
    - match: list
    - musthaves.laptop

  # Saltmasters:
  tiger,jastest,jas,sugar,saltmaster,crystal,debian,manjaroxfce:
    - match: list
    - salt.master
    # salt.virt
    - salt.ssh

  saltmaster:
    - match: list
    - developjassalt

  # Barrier Nodes:
  tiger,sugar,crystal,modern:
    - match: list
    - musthaves.barrier
  # Need printing:
    - cups.install

  # Desktops with commercial virtualization:
  tiger,jas,sugar,crystal,:
    - match: list
    - virtualbox
    - vmware.workstationpro

# TODO:  change comment from KVM
# TODO:  modern is ThoroughCare, create thoroughcareemployee.sls w/ ff:
  # Desktops with KVM virtualization:
  modern:
    - match: list
# TODO:  not working 2024-11-07 14:16:22-05:00 --> gitfs working?
    - awscli
    - developruby
    - githubcli
#    - libvirt
#    - salt.virt
    - docker

  # VMware systems:
  sugar,crystal:
    - match: list
    - vmware.employee

  # 'store' is an openmediavault NAS:
  store:
    - omv
    - openmediavault
    - musthaves
