{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,
        stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        bootMntPt,
        mntPt,
        sysMntPt,
        with context %}
{%   set devMntPt = mntPt ~ "/dev" %}

{{sls}} - Unmount sys mountpoints:
  mount.unmounted:
    - names:
      - {{sysMntPt}}/kernel/config
      - {{sysMntPt}}/kernel/debug
      - {{sysMntPt}}/kernel/security
      - {{sysMntPt}}/fs/fuse/connections
      - {{sysMntPt}}/firmware/efi/efivars
      - {{sysMntPt}}/fs/cgroup/openrc
      - {{sysMntPt}}/fs/cgroup/unified

{{sls}} - Unmount cgroup mountpoints:
  mount.unmounted:
    - name: {{sysMntPt}}/fs/cgroup
    - require:
      - {{sls}} - Unmount sys mountpoints

{{sls}} - Unmount {{sysMntPt}}:
  mount.unmounted:
    - name: {{sysMntPt}}
    - require:
      - {{sls}} - Unmount cgroup mountpoints

{{sls}} - Unmount dev mountpoints:
  mount.unmounted:
    - names:
      - {{devMntPt}}/shm
      - {{devMntPt}}/pts
      - {{devMntPt}}/mqueue

{{sls}} - Unmount {{devMntPt}}:
  mount.unmounted:
    - name: {{devMntPt}}
    - require:
      - {{sls}} - Unmount dev mountpoints

{{sls}} - Unmount {{mntPt}}/proc:
  mount.unmounted:
    - name: {{mntPt}}/proc

{{sls}} - Unmount {{bootMntPt}}:
  mount.unmounted:
    - name: {{bootMntPt}}

{{sls}} - Unmount {{mntPt}}:
  mount.unmounted:
    - name: {{mntPt}}
    - require:
      - {{sls}} - Unmount {{sysMntPt}}
      - {{sls}} - Unmount {{devMntPt}}
      - {{sls}} - Unmount {{mntPt}}/proc
      - {{sls}} - Unmount {{bootMntPt}}

{% endif %}  # hostnameIsCorrect
