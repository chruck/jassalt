{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,
        stateNotInstalling,
        with context %}
{% from tpldir ~ "/iscontainer.jinja" import is_container with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% elif is_container %}

{{sls}} - Running in container:
  test.succeed_with_changes:
    - name: Not installing grub inside a container

{% else %}  # Not a container:

{%   from tpldir ~ "/vars.jinja" import
                filesystemtype,
                mapperName,
                formatDisk,
                with context %}

{# set luksPasswd = salt['pillar.get']('luks:passwd', 'passwd') #}

#{{sls}} - Label {{pillar.funtoodisk}} as GPT:
#  module.run:
#    - name: partition.mklabel
#    - device: {{pillar.funtoodisk}}
#    - label_type: gpt
#    - label_type: msdos

#{{sls}} - Create partition {{pillar.funtoodisk}}1 as fat32:
#  module.run:
#    - name: partition.mkpart
#    - device: {{pillar.funtoodisk}}
#    - part_type: primary
#    - fs_type: fat32
#    - start: 2048s
#    - end: 534527s
#    - start: 0
#    - end: 260

#{{sls}} - Create partition {{pillar.funtoodisk}}2 as {{filesystemtype}}:
#  module.run:
#    - name: partition.mkpart
#    - device: {{pillar.funtoodisk}}
#    - part_type: primary
#    - fs_type: {{filesystemtype}}
#    - start: 534528s
#    - end: -1s
#    - end: 1000215182s
#    - start: 260

{# Postpone using LUKS, for now:
{{sls}} - Format {{pillar.funtoodiskslash}} as LUKS:
  cmd.run:
#    - name: cryptsetup luksFormat --cipher aes-xts-plain64 --hash sha512 --key-size 512 {{pillar.funtoodiskslash}}
#zettaknight:    - name: cryptsetup luksFormat --cipher aes-xts-plain --hash sha256 --key-size 512 {{pillar.funtoodiskslash}}
    - name: echo "{{pillar.luks.passwd}}" | cryptsetup luksFormat --cipher aes-xts-plain64 {{pillar.funtoodiskslash}}

# to undo:
#    cryptsetup luksRemoveKey /dev/sda <<<"{{pillar.luks.passwd}}"
#    umount /mnt/funtoo/sys/fs/cgroup/*
#    umount /mnt/funtoo/sys/fs/*
#    umount /mnt/funtoo/sys/
#    umount /mnt/funtoo/
#    cryptsetup remove /dev/mapper/{{mapperName}}
{{sls}} - Make {{pillar.funtoodiskslash}} a mapper device named {{mapperName}}:
  cmd.run:
    - name: echo "{{pillar.luks.passwd}}" | cryptsetup luksOpen {{pillar.funtoodiskslash}} {{mapperName}}
    - require:
      - {{sls}} - Format {{pillar.funtoodiskslash}} as LUKS
#}  # postpone LUKS

#{{sls}} - Format {{pillar.funtoodiskslash}} (cmd.run):
#  cmd.run:
#    - name: "mkfs.{{filesystemtype}} {{pillar.funtoodiskslash}} -f"

{#
{{sls}} - Format {{pillar.funtoodiskslash}} (module.run):
  module.run:
    - name: {{filesystemtype}}.mkfs
    - devices:
      - {{pillar.funtoodiskslash}}
    - require:
      - {{sls}} - Make {{pillar.funtoodisk}} a mapper device named {{mapperName}}
#}

{{formatDisk}}:
  blockdev.formatted:
    - name: {{pillar.funtoodiskslash}}
    - fs_type: {{filesystemtype}}
    - force: True
#    - require:
#      - {{sls}} - Format {{pillar.funtoodiskslash}} (cmd.run)
#      - {{sls}} - Format {{pillar.funtoodiskslash}} (module.run)
#      - {{sls}} - Make {{pillar.funtoodiskslash}} a mapper device named { {mapperName}}

{% endif %}    # hostnameIsCorrect, grain.virtual (container?)
