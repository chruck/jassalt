# TODO:  Replace with salt formula:
#        https://github.com/salt-formulas/salt-formula-virtualbox
#        Not replacing right now, because Deb version uses Virtualbox
#        repo instead of Debian Fast Track.  --> maybe contrib that to
#        formula?
#        Also, that formula may need update for latest version:
#        currently supports 4 & 5, 6.1.34 is latest

{% from tpldir ~ "/map.jinja" import vboxpkgs with context %}

include:
  - .redhatfamily
  - .debianfamily

{{sls}} - Install VirtualBox packages:
  pkg.installed:
    - pkgs: {{vboxpkgs}}
