# salt/installArchAtopFuntoo/createFakeDisk.sls

# State to create a fake disk from an empty file.
# - For use by a _local_ container to put a btrfs filesystem on.
# NOTE:  'module.run' of 'btrfs.mkfs' will fail in a
#        non-'--privileged' container, esp. GitlabCI

{% set diskfile = "/builds/chruck/jassalt/btrfs.fakedisk" %}

include:
  - .identifyAsGentoo
  - .installBtrfs

{{sls}} - Touch {{diskfile}}:
  file.touch:
    - name: {{diskfile}}
    - onlyif: "! test -f {{diskfile}}"

{{sls}} - Set {{diskfile}} to a sparse file:
  module.run:
#    - name: file.truncate
#    - length: {{1024*1024*1024}}
    - file.truncate:
      - path: {{diskfile}}
      - length: {{1024*1024*1024}}
#    - onlyif: test -f {{diskfile}}
    - require:
      - {{sls}} - Touch {{diskfile}}

{{sls}} - mkfs on {{diskfile}}:
#  btrfs.properties:
  module.run:
    - btrfs.mkfs:
      - {{diskfile}}
#      - name:
#        - {{diskfile}}
#      - name: {{diskfile}}
#      - devices: {{diskfile}}
#      - m_devices: {{diskfile}}
#      - devices:
#        - {{diskfile}}
#      - args: {{diskfile}}
#      - args:
#        - {{diskfile}}
#      - args:
#        - devices:
#          - {{diskfile}}
#    - onlyif: test -f {{diskfile}}
    - require:
      - {{sls}} - Set {{diskfile}} to a sparse file
