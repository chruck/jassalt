# salt/obsidian.sls

{% if "Debian" != grains.os_family %}

# TODO:  AppImage and Snap are at:
# https://github.com/obsidianmd/obsidian-releases/releases/

{{sls}} - Only on Debian family:
  test.nop

{% else %}  # Debian family

{%   set obsidianrelease = "1.6.7" %}
{%   set deburl =
        "https://github.com/obsidianmd/obsidian-releases/releases/download/v"
        ~ obsidianrelease ~ "/obsidian_"
        ~ obsidianrelease ~ "_amd64.deb"
%}

{{sls}} - Install .deb of Obsidian:
  pkg.installed:
    - sources:
      - obsidian: {{deburl}}

{% endif %}  # Debian family
