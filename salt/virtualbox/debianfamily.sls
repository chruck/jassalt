# Install VirtualBox on Debian-family systems

include:
  - .install

{% if "Debian" == grains.os_family %}  # Debian-family
{%   if "Debian" == grains.os %}  # Debian-only

# https://fasttrack.debian.net/
# NOTE:  Above has other instructions for Buster (below is Bullseye)

{%     if "bullseye" == grains.oscodename %}  # Debian 11 only
{{sls}} - Install Debian backports:
  pkgrepo.managed:
    - humanname: Debian Backports repo
    - name: deb http://deb.debian.org/debian bullseye-backports main
    - file: /etc/apt/sources.list.d/backports.list
    - gpgcheck: 1
    - require_in:
      - {{sls}} - Install Debian Fast Track repo
{%     endif %}  # Bullseye/Debian 11

{{sls}} - Install Debian Fast Track repo keyring:
  pkg.installed:
    - pkgs:
      - fasttrack-archive-keyring

{{sls}} - Install Debian Fast Track repo:
  pkgrepo.managed:
    - humanname: Debian Fast Track repo
    - name: deb https://fasttrack.debian.net/debian-fasttrack/ {{grains.oscodename}}-fasttrack main contrib
    - file: /etc/apt/sources.list.d/fasttrack.list
    - gpgcheck: 1
    - require:
      - {{sls}} - Install Debian Fast Track repo keyring

{{sls}} - Install Debian Fast Track backports repo:
  pkgrepo.managed:
    - humanname: Debian Fast Track backports repo
    - name: deb https://fasttrack.debian.net/debian-fasttrack/ {{grains.oscodename}}-backports-staging main contrib
    - file: /etc/apt/sources.list.d/fasttrackbackports.list
    - gpgcheck: 1
    - require:
      - {{sls}} - Install Debian Fast Track repo
    - require_in:
      - virtualbox.install - Install VirtualBox packages

{%   endif %}  # Debian-only

{{sls}} - Install apt-utils for VirtualBox package:
  pkg.installed:
    - name: apt-utils
    - require_in:
      - virtualbox.install - Install VirtualBox packages

# echo virtualbox-ext-pack virtualbox-ext-pack/license select true \
#              | sudo debconf-set-selections
# NOTE:  May need debconf-utils package
{{sls}} - Pre-accept VirtualBox Ext-Pack license:
  debconf.set:
    - name: virtualbox-ext-pack
    - data:
        virtualbox-ext-pack/license: {'type': 'boolean', 'value': true}
#        virtualbox-ext-pack/license:
#          - type: boolean
#          - value: true
    - require_in:
      - virtualbox.install - Install VirtualBox packages

{% endif %}    # Debian-family
