# salt/adduser.sls

include:
  - gentoo.sudo

#{{sls}} - Create group '{{pillar.user}}':
#  group.present:
#    - name: {{pillar.user}}
#    - members:
#      - {{pillar.user}}

# TODO:  create multiple users (eg, `jas` and `rocker` for VMware)

{{sls}} - Create user '{{pillar.user}}':
  user.present:
    - name: {{pillar.user}}
    - fullname: Jas
    - shell: /bin/bash
    - password: $6$mLWkWhz7$XHI6MwUCXWVmrhwge2E7yOq/USreFgLu.Z1TW/c5qjCY4wwtN3vRdAmKu8PAzaUKIn8yXqkx1ykE7jCzx3BVZ/
    - enforce_password: False
    - remove_groups: False
    - usergroup: True
    - optional_groups:
      - adm
      - libvirt
      - libvirt-qemu
      - plugdev
      - salt
      - sudo
      - users
      - wheel

{% if "Debian" != grains.os_family %}
{{sls}} - Create directory if not already:
  file.directory:
    - name: /etc/sudoers.d

{{sls}} - Give {{pillar.user}} full sudo access:
  file.managed:
    - name: /etc/sudoers.d/{{pillar.user}}
    - contents:  {{pillar.user}} ALL=(ALL) ALL
    - require:
      - {{sls}} - Create directory if not already
{% endif %}  # non-Debian
