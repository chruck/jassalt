# salt/installArchAtopFuntoo/init.sls

# The full set of states to install Arch on top of a Funtoo system,
# based on
# https://wiki.archlinux.org/title/Install_from_existing_Linux

#include:
#  - .createChroot

placeholder:
  test.nop
