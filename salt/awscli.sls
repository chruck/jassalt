# salt/awscli.sls

# Install the AWS commandline `awscli` package for the `aws` command.
# Probably behind the latest version.

{{sls}} - Install awscli pkg:
  pkg.installed:
    - name: awscli
