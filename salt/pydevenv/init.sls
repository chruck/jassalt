# States to setup a Python developer environment ("pydevenv").
# Originally for use at VMware/Salt for SecOps Content, based on
# https://confluence.eng.vmware.com/pages/viewpage.action?spaceKey=saltstackinchome&title=Content+Development+Environment+Setup

include:
  - .prereqs
  - .pyenv
#  - .pytest
