{% from "salt/map.jinja" import atinstallID, minionsvcID with context %}

include:
  - .at

{{minionsvcID}}:
  at.watch:
    - job: 'salt-call --local service.restart salt-minion'
    - timespec: 'now +1 min'
    - tag: minion_restart
    - unique_tag: true
    - require:
      - {{atinstallID}}
