# Based on notes from Salt's CONTRIBUTING.rst

{{sls}} - Checkout pyenv from GitHub:
  git.latest:
    - name: https://github.com/pyenv/pyenv.git
    - target: /home/jas/.pyenv
    - depth: 1

{{sls}} - Checkout pyenv-virtualenv from GitHub:
  git.latest:
    - name: https://github.com/pyenv/pyenv-virtualenv.git
    - target: /home/jas/.pyenv
    - depth: 1
