{% from tpldir ~ "/map.jinja" import musthaves with context %}

{% if musthaves.desktoppip is defined %}

# TODO:  Refactor this with the salt version of installing pip
{{sls}} - Install pip:
  pkg.installed:
    - name: python3-pip

{{sls}} - Install musthaves via pip:
  pip.installed:
    - names: {{musthaves.desktoppip}}
    - bin_env: /usr/bin/pip
    - require:
      - {{sls}} - Install pip

{% endif %}  # desktoppip
