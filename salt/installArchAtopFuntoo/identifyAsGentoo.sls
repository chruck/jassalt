# salt/installArchAtopFuntoo/identifyAsGentoo.sls

# State to change Funtoo system to identify as Gentoo (not just be in
# family Gentoo).
# NOTE:  In order for salt to change its grains, the minion must be
#        restarted.

{{sls}} - Change identity to _g_entoo:
  file.replace:
    - name: /etc/os-release
    - pattern: funtoo
    - repl: gentoo

{{sls}} - Change identity to _G_entoo:
  file.replace:
    - name: /etc/os-release
    - pattern: Funtoo
    - repl: Gentoo
