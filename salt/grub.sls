# salt/grub.sls

# Output of Grub command `videoinfo`:
#
# Dell Precision 5560 (VMware laptop, first when FTE, "crystal",
# service tag CBKH453)
# ---
# List of supported video modes:
# Legend: mask/position=red/green/blue/reserved
# Adapter: `Bochs PCI Video Driver':
#   No info available
# Adapter: `Cirrus CLGD 5446 PCI Video Driver':
#   No info available
# Adapter: `EFI GOP driver':
# * 0x000 3840 x 2400 x 32 (15360)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x001  640 x  480 x 32 (2560)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x002  800 x  600 x 32 (3200)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x003 1024 x  768 x 32 (4096)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x004 1280 x 1024 x 32 (5120)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x005 1600 x 1200 x 32 (6400)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x006 1920 x 1080 x 32 (7680)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x007 1920 x 1440 x 32 (7680)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   EDID version: 1.4
#     Preferred mode: 3840x2400
# ---
# Dell XPS 13 9340 (ThoroughCare laptop, "modern", service tag DXKNB54)
# ---
# List of supported video modes:
# Legend: mask/position=red/green/blue/reserved
# Adapter: `Bochs PCI Video Driver':
#   No info available
# Adapter: `Cirrus CLGD 5446 PCI Video Driver':
#   No info available
# Adapter: `EFI GOP driver':
# * 0x000 2560 x 1600 x 32 (10240)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x001  640 x  480 x 32 (2560)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x002  800 x  600 x 32 (3200)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x003 1024 x  768 x 32 (4096)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x004 1280 x  720 x 32 (5120)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x005 1280 x 1024 x 32 (5120)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x006 1600 x 1200 x 32 (6400)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x007 1920 x 1080 x 32 (7680)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   0x008 1920 x 1440 x 32 (7680)  Direct color, mask: 8/8/8/8  pos: 16/8/0/24
#   EDID version: 1.4
#     Preferred mode: 2560x1600
# ---

{% from tpldir ~ "/iscontainer.jinja" import is_container with context %}
{% from tpldir ~ "/map.jinja" import updategrubcmd with context %}
{% from tpldir ~ "/pkgnames_map.jinja" import pkg_name, with context %}

{% set dontwants = [
        "quiet",
        "rhgb",
        "splash",
        "zswap.compressor=\w*",
        "zswap.enabled=.",
     ]
%}

{% if is_container %}

{{sls}} - Generate /etc/default/grub:
  pkg.installed:
    - name: {{pkg_name.grub}}
    - onlyif: test -f /etc/default/grub
    - require_in:
{%   for dontwant in dontwants %}
      - {{sls}} - Remove '{{dontwant}}'
{%   endfor %}  # dontwant's
      - {{sls}} - Play Star Wars Imperial March in GRUB
      - {{sls}} - Add zswap to kernel commandline

{% endif %}  # not for containers

{% for dontwant in dontwants %}

{{sls}} - Remove '{{dontwant}}':
  file.replace:
    - name: /etc/default/grub
    - pattern: \s*{{dontwant}}\s*
    - repl: " "
    - onlyif: test -f /etc/default/grub
    - onchanges_in:
      - {{sls}} - Rerun grub

{% endfor %}  # dontwant's

{% if salt.pkg.version("dracut") %}

{{sls}} - Add lz4 to dracut:
  file.managed:
    - name: /etc/dracut.conf.d/lz4.conf
    - contents: 'add_drivers+="lz4 lz4_compress"'

{# TODO?
{% if is_container %}
#}

{{sls}} - Regenerate initramfs files w/ dracut:
  cmd.run:
    - name: dracut --regenerate-all --force
    - watch:
      - {{sls}} - Add lz4 to dracut
{%   for dontwant in dontwants %}
      - {{sls}} - Remove '{{dontwant}}'
{%   endfor %}  # dontwant's

{# TODO?
{% endif %}  # is_container
#}

{% endif %}  # dracut

# TODO:  Look into replacing these with /etc/default/grub.d files:

# TODO:  Possibly replace zswap with zram or similar:
# Not sure, this step may be Fedora-only:
{{sls}} - Add zswap to kernel commandline:
  file.replace:
    - name: /etc/default/grub
    - pattern: '^(GRUB_CMDLINE_LINUX="[^"]*)'
    - repl: '\1 zswap.enabled=1 zswap.compressor=lz4 '
    - onlyif: test -f /etc/default/grub

# See table at the top of this file:
{% if grains.id in [ "crystal", "modern" ] %}

{%   set mode = '640x480' %}
{%   if "modern" == grains.id %}
{%     set mode = '1600x1200' %}
{%   endif %}  # graphics modes for "modern"

{{sls}} - Change font resolution in GRUB:
  file.replace:
    - name: /etc/default/grub
    - pattern: '^#?GRUB_GFXMODE.*$'
    #- repl: 'GRUB_GFXMODE=1920x1440'
    - repl: 'GRUB_GFXMODE={{mode}}'
    - onlyif: test -f /etc/default/grub
    - onchanges_in:
      - {{sls}} - Rerun grub

{% endif %}  # crystal and modern

# via http://wiki.thorx.net/wiki/GRUB :
{{sls}} - Play Star Wars Imperial March in GRUB:
  file.replace:
    - name: /etc/default/grub
    - pattern: '^#?GRUB_INIT_TUNE.*$'
    #- repl: 'GRUB_INIT_TUNE="480 440 4 440 4 440 4 349 3 523 1 440 4 349 3 523 1 440 8 659 4 659 4 659 4 698 3 523 1 415 4 349 3 523 1 440 8"'
    - repl: 'GRUB_INIT_TUNE="480 440 4 440 4 440 4 349 3 523 1 440 4 349 3 523 1 440 8"'
    - onlyif: test -f /etc/default/grub

{#
{% if not is_container %}
#}

{{sls}} - Rerun grub:
  cmd.run:
    - name: {{updategrubcmd}}
    - onchanges:
      - {{sls}} - Play Star Wars Imperial March in GRUB
      - {{sls}} - Add zswap to kernel commandline

{#
{% endif %}  # not for containers
#}
