{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}

include:
  - .prepareDisk
  - .mountingFilesystems
  - .settingTheDate
  - .installingTheStage3Tarball
  - .mountVirtFs
  - .chrootIntoFuntoo
  - .downloadingThePortageTree
  - .configurationFiles-fstab
  - .configurationFiles-localtime
#  - .configurationFiles-makeConf
  - .updatingWorld
  - .installingABootloader
#  - .oldSchoolBiosMbr
  - .newSchoolUefiBootEntry
  - .configuringYourNetwork-Wi-Fi
  - .configuringYourNetwork-Hostname
  - .setYourRootPassword
  - .installSalt
  - .patchFakeFuntooIsGentoo
  - .umountSda
  - .restartYourSystem

{% endif %}  # hostnameIsCorrect
