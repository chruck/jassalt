# salt/backports.sls

# Add Debian "Backports" repo, then disable after other states have
# used it.

# Originally from salt/virtualbox/debianfactory.sls

# TODO:  refactor virtualbox.debianfactory to use this file

{% if "Debian" == grains.os %}  # Debian only

# https://fasttrack.debian.net/
{#
# NOTE:  Above has other instructions for Buster (below is Bullseye)

{%     if "bullseye" == grains.oscodename %}  # Debian 11 only
#}
{{sls}} - Install Debian backports:
  pkgrepo.managed:
    - humanname: Debian Backports repo
    - name: deb http://deb.debian.org/debian {{grains.oscodename}}-backports main
    - file: /etc/apt/sources.list.d/backports.list
    - gpgcheck: 1
    - order: 1
#    - enabled: False
    - disabled: True
{#
    - require_in:
      - {{sls}} - Install Debian Fast Track repo
{%     endif %}  # Bullseye/Debian 11
#}

{#
{{sls}} - Install Debian Fast Track repo keyring:
  pkg.installed:
    - pkgs:
      - fasttrack-archive-keyring
#}

{#
{{sls}} - Install Debian Fast Track repo:
  pkgrepo.managed:
    - humanname: Debian Fast Track repo
    - name: deb https://fasttrack.debian.net/debian-fasttrack/ {{grains.oscodename}}-fasttrack main contrib
    - file: /etc/apt/sources.list.d/fasttrack.list
    - gpgcheck: 1
    - order: 2
    - require:
      - {{sls}} - Install Debian Fast Track repo keyring

{{sls}} - Install Debian Fast Track backports repo:
  pkgrepo.managed:
    - humanname: Debian Fast Track backports repo
    - name: deb https://fasttrack.debian.net/debian-fasttrack/ {{grains.oscodename}}-backports-staging main contrib
    - file: /etc/apt/sources.list.d/fasttrackbackports.list
    - gpgcheck: 1
    - order: 3
    - require:
      - {{sls}} - Install Debian Fast Track repo
    - require_in:
      - virtualbox.install - Install VirtualBox packages
#}

{{sls}} - Disable Debian backports:
  pkgrepo.managed:
    - humanname: Debian Backports repo
    - name: deb http://deb.debian.org/debian {{grains.oscodename}}-backports main
    - enabled: False
    - order: last

{#
{{sls}} - Disable Debian Fast Track repo:
  pkgrepo.managed:
    - humanname: Debian Fast Track repo
    - name: deb https://fasttrack.debian.net/debian-fasttrack/ {{grains.oscodename}}-fasttrack main contrib
    - enabled: False
    - order: last
#}

{#
{{sls}} - Disable Debian Fast Track backports repo:
  pkgrepo.managed:
    - humanname: Debian Fast Track backports repo
    - name: deb https://fasttrack.debian.net/debian-fasttrack/ {{grains.oscodename}}-backports-staging main contrib
    - enabled: False
    - order: last
#}

{% endif %}  # Debian only
