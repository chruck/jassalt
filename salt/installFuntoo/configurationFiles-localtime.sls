{% from tpldir ~ "/hostnameIsCorrect.jinja" import
        hostnameIsCorrect,stateNotInstalling
        with context %}

{% if not hostnameIsCorrect %}

{{ stateNotInstalling }}

{% else %}  # hostnameIsCorrect

{%   from tpldir ~ "/vars.jinja" import
        mntPt,
        mountAsFuntoo,
        mountingFilesystems,
        localtimeFile,
        timezoneFile,
        with context %}

include:
  - {{mountingFilesystems}}

{{sls}} - Create symlink for {{localtimeFile}}:
  cmd.run:
    - name: /bin/chroot {{mntPt}} ln -sf {{timezoneFile}} {{localtimeFile}}
    - require:
      - {{mountAsFuntoo}}

{% endif %}  # hostnameIsCorrect
